# Lektion 5
# Avancerade funktioner i GitLab: Grenstrategier

I den här lektionen kommer vi att studera avancerade funktioner avseende grenstrategier. Grenhantering är en viktig del av ett effektivt arbetsflöde i GitLab och kan hjälpa dig att organisera arbetet och samarbeta smidigt med andra utvecklare. Vi kommer att titta på olika grenmodeller, bästa praxis för grenhantering och använda GitLabs verktyg för grenvisualisering.

## Översikt över grenstrategier

Grenstrategier är en plan för att organiserara arbetsflödet med hjälp av Git. Dessa strategier definierar hur du använader grenar (branches) för att separera olika delar av ett projekt och hantera ändringar. Låt oss utforska några vanliga grenstrategier.

### Funktionsgrenar (Feature Branches)

Funktionsgrenar är en vanlig grenstrategi där varje ny funktion eller uppgift som ska implementeras får en egen gren. Denna strategi hjälper till att isolera ändringar och nya funktioner från huvudgrenen tills de är klara och testade. När funktionen är klar, sammanfogas funktionsgrenen med huvudgrenen.

Antag att du arbetar med en webbapplikation och behöver implementera en ny användarregistrering. Istället för att göra dessa ändringar direkt i huvudgrenen, kan du skapa en funktionsgren med namnet "user-registration". Du arbetar sedan i denna grenen tills funktionen är klar och testad. När den är redo, sammanfogar du den tillbaka till huvudgrenen.

För att skapa en ny funktionsgren och byta till den:

```shell
# Create a new feature branch
git checkout -b feature/user-registration

# Work in your feature branch and make commits

# When done, go back to the main branch
git checkout main

# Merge the feature branch into the main branch
git merge feature/user-registration
```

### Leveransgrenar (Release Branches)

Leveransgrenar används för att förbereda en ny version eller leverans av projektet. Dessa grenar används ofta för att åtgärda programfel (buggar) och förbereda projektet för leverans. När leveransen är klar, sammanfogas den tillbaka till huvudgrenen och ibland även till andra grenar exempelvis funktionsgrenar.

Om du utvecklar programvara och är redo att släppa en ny version, kan du skapa en leveransgren med versionsnumret (t.ex., "v1.0"). Du arbetar sedan i den grenen för att göra de sista ändringarna, testa och åtgärda eventuella buggar. När leveransen är klar, sammanfogar du leveransgrenen med huvudgrenen och kanske med andra grenar som behöver de nya funktionerna.

För att skapa en leveransgren och sammanfoga den:

```shell
# Create a new release branch
git checkout -b release/v1.0

# Work in your release branch and make commits

# When done, return to the main branch
git checkout main

# Merge the release branch into the main branch
git merge release/v1.0
```

### Snabblagningsgrenar (Hotfix Branches)

Snabblagningsgrenar används för att snabbt åtgärda akuta problem eller buggar som har uppstått i produktionen. Dessa grenar skapas direkt från huvudgrenen, och när problemet är löst, sammanfogas de tillbaka med både huvudgrenen och eventuella leveransgrenar.

Om exempelvis din webbplats är driftsatt och du upptäcker en allvarlig säkerhetsbugg, kan du snabbt skapa en snabblagningsgren från huvudgrenen. Du arbetar i snabblagningsgrenen för att åtgärda problemet och testa lösningen. När det är klart, sammanfogas snabblagningsgrenen med huvudgrenen och alla aktuella leveransgrenar.

För att skapa en snabblagningsgren och sammanfoga den med huvudgrenen:

```shell
# Create a new hotfix branch
git checkout -b hotfix/emergency-bug-fix

# Work in your hotfix branch and make commits

# When done, return to the main branch
git checkout main

# Merge the hotfix branch into the main branch
git merge hotfix/emergency-bug-fix
```

## Bästa praxis för grenhantering

Att följa bästa praxis för grenhantering kan göra ditt arbete smidigare och mer organiserat. Här är några tips:

- **Ge meningsfulla namn åt dina grenar:** Använd beskrivande namn som tydligt anger syftet med grenen. Det gör det enklare för dig och andra att förstå vad som pågår.

- **Håll dina grenar uppdaterade:** Sammanfoga regelbundet dina arbetsgrenar med den senaste koden från huvudgrenen för att undvika konflikter och hålla dig synkroniserad.

För att uppdatera din aktiva gren med senaste ändringar från huvudgrenen:

```shell
# Switch to your active branch
git checkout your-branch

# Pull the latest changes from the main branch
git pull origin main
```

**Använd Pull Requests (Förfrågningar om att sammanfoga ändringar):** Om möjligt, använd GitLabs Pull Request-funktion (även kallad Merge Request) för att begära granskning och sammanfogning av ändringar. Detta underlättar granskningsprocessen och möjliggör att diskutera ändringarna innan de sammanfogas.

Här är ett exempel på hur du skapar en Pull Request (även kallad Merge Request) i GitLab:

1. **Skapa en ny gren:** Först, skapa en ny gren för din funktion eller buggfix. Du kan göra detta med följande kommando:

```shell
# Create a new branch
git checkout -b feature-branch
```

2. **Arbeta med dina ändringar:** Gör dina kodändringar och commits i den aktuella grenen.

3. **Pusha din gren:** När du är redo att dela dina ändringar och begära en granskning, pusha din gren till fjärrkodförrådet på GitLab:

```shell
# Push the branch to GitLab
git push origin feature-branch
```

4. **Skapa en Pull Request:** Gå till GitLabs projektsida och navigera till avsnittet "Merge Requests" eller "Pull Requests." Klicka på knappen "New Merge Request" eller "New Pull Request."

5. **Välj grenar:** Välj källgrenen (din funktionsgren) och målgrenen (vanligtvis "main" eller "master").

6. **Lägg till beskrivning:** Ange en meningsfull titel och beskrivning för din Pull Request, där du förklarar vilka ändringar du har gjort och varför.

7. **Granskare:** Tilldela en eller flera granskare som kommer att granska dina kodändringar.

8. **Granskning och diskussion:** Dina granskare kommer att granska dina ändringar, och ni kan föra diskussioner om eventuella frågor eller förslag direkt inom aktuell Pull Request.

9. **Sammanfoga:** När dina ändringar har granskats och godkänts, kan Pull Requesten sammanfogas med målgrenen, vanligtvis "main" eller "master."

10. **Ta bort grenen:** Efter sammanfogningen kan du välja att ta bort din gren för att hålla din kodbas i gott skick:

```shell
# Delete the local branch (if you're done with it)
git branch -d feature-branch

# Delete the remote branch
git push origin --delete feature-branch
```

Att använda Pull Requests (eller Merge Requests) i GitLab förenklar kodgranskningsprocessen, underlättar samarbete och säkerställer att ändringar granskas noggrant innan de sammanfogas med huvudgrenen. Det är en bästa praxis för samarbetsutveckling.

**Använda GitLabs verktyg**

GitLab erbjuder verktyg för att skapa förståelse för hur ditt projekt utvecklas över tid. Att exempelvis kunna se hur olika grenar i ditt projekt förhåller sig till varandra är värdefullt vid projekthantering. Genom att använda GitLabs verktyg kan du enkelt följa ditt projekts utveckling, se hur olika funktioner och ändringar integreras och identifiera eventuella områden som behöver uppmärksamhet. Det ger dig en djupare förståelse för din projektstruktur och hjälper till att hålla ordning på arbetet.

Att använda GitLabs verktyg är särskilt värdefullt i större projekt med många grenar och utvecklingsteam. Det ger hela teamet insikt i hur arbetet utvecklats och hur ändringar påverkat huvudgrenen.

### Uppgift

För att fördjupa din förståelse för GitLabs grenar, utforska de olika vyerna och filteralternativen som erbjuds. Prova att hitta specifika grenar, se hur de utvecklas över tid och försök förutse hur ändringar i en gren kan påverka andra delar av projektet. Detta kommer att hjälpa dig att bli en mer kraftfull GitLab-användare när det gäller projektöversikt och projekthantering.

Föregående: [Samarbetsbaserad utveckling med GitLab](gitlab_s4.md)


# Lektion 4

# Ansible Playbooks och Automatisering

I den här fjärde lektionen fördjupar vi oss i Ansible Playbooks. Vi studerar playbook-strukturen och syntaxen, skapar och kör en grundläggande playbook.

## Förstå playbook-strukturen och syntaxen

Ansible Playbooks är hjärtat i Ansible-automatiseringen, där vi beskriver våra automatiserade uppgifter. Playbooks är skrivna i YAML-format, och följer en tydlig struktur för att definiera automatiserade uppgifter.

De centrala sektionerna i en Ansible-playbook är:

- **Hosts (Värdar):** Här specificerar vi de målenheter där vi vill utföra våra uppgifter. Dessa målenheter kan anges som enskilda IP-adresser, en lista av värdnamn eller i grupper. 

- **Tasks (Uppgifter):** Tasks-sektionen definierar de faktiska automatiserade uppgifterna som ska utföras på våra målenheter. Det är här vi beskriver vilka handlingar som ska utföras, som att installera paket, kopiera filer eller konfigurera tjänster.

- **Handlers (Hanterare):** Handlers är snarlika uppgifter (tasks), men de aktiveras endast om en tidigare utförd uppgift har ändrat systemets tillstånd. Till exempel, om vi installerar en webbserver, kan en hanterare användas för att starta tjänsten om installationen lyckades.

- **Variables (Variabler):** Variabler används för att parametrisera våra playbooks. De gör det möjligt att återanvända en playbook på olika målenheter eller för att ändra uppgiftsbeteenden beroende på variablernas värde.

- **Defaults (Standardvärden):** Denna sektion innehåller standardvärden som kan användas i playbooks. Det gör det möjligt att centralisera standardkonfigurationer och undvika upprepning.

- **Templates (Mallar):** Templates används för att generera konfigurationsfiler eller textbaserade resurser dynamiskt. De kan innehålla platshållare för variabler och är särskilt användbara för att konfigurera programvara och tjänster med skräddarsydda inställningar.

Dessa sektioner utgör grunden för att definiera och organisera automatiserade uppgifter med Ansible Playbooks.

## Skapa och exekvera en playbook

Nedan visas en enkel Ansible-playbook i YAML-format. Denna playbook installerar och startar en Apache webbserver.

```yaml
---
- name: Install a webserver
  hosts: web
  tasks:
    - name: Install Apache
      dnf:
        name: httpd
        state: present
    - name: Start Apache
      systemd:
        name: httpd
        enabled: yes
        state: started

```

I det här exemplet har vi:

- **name:** En beskrivning av playbooken och dess syfte.
- **hosts:** Den grupp av målenheter där uppgifterna ska utföras.
- **tasks:** De faktiska uppgifterna som ska utföras, i det här fallet installation och start av Apache-webbservern.

För att kontrollera syntax använder du följande kommando:

```shell
ansible-playbook -i path/to/inventory.yml --syntax-check apache_playbook.yml
```

För att köra playbooken använder du följande kommando:

```shell
ansible-playbook -i path/to/inventory.yml apache_playbook.yml
```

## Hantera variabler och fakta i playbooks

Att hantera variabler och fakta i Ansible Playbooks är viktigt. Med variabler blir dina automatiserade uppgifter mer flexibla och anpassningsbara.

### Variabler i playbooks

Variabler i Ansible tillåter oss att parametrisera våra playbooks och göra dem återanvändbara. Vi kan definiera variabler på flera nivåer, inklusive i själva playbooken, i roller, eller till och med i separata variabelfiler. Detta gör det möjligt att enkelt ändra konfigurationer och beteenden utan att ändra själva playbooken.

Här är ett exempel på hur vi kan använda variabler för att installera webbservern Nginx:

```
---
- name: Install Nginx
  hosts: web
  vars:
    nginx_package: nginx
  tasks:
    - name: Install Nginx
      dnf:
        name: "{{ nginx_package }}"
        state: present
    - name: Start Nginx
      systemd:
        name: nginx
        enabled: yes
        state: started
```

I detta exempel har vi definierat en variabel nginx_package som innehåller namnet på paketet att installera. Genom att använda dubbla klammerparenteser {{ }} i dnf-uppgiften ersätts variabeln med dess värde. Det är en bra praxis att använda den lämpliga modulen (service eller systemd) baserat på init-systemet som används av målsystemet för att säkerställa kompatibilitet.

### Fakta i playbooks

Fakta (facts) i Ansible är information om din målenhet som samlas in av Ansible automatiskt. Fakta ger dig insikt i målenhetens nuvarande tillstånd och kan vara bra att använda för att skapa effektiva och flexibla playbooks. Exempel på fakta inkluderar information som operativsystemstyp, maskinvara och nätverkskonfiguration. Genom att använda fakta i dina playbooks kan du skapa automatiserade uppgifter som anpassar sig dynamiskt till målsystemets egenskaper.

Låt oss säga att du vill konfigurera Nginx på dina Linux-servrar, men installationspaketet varierar beroende på operativsystemstyp. Med hjälp av fakta kan du skapa en dynamisk playbook som väljer rätt paket att installera baserat på fakta om operativsystemet.

Här är ett exempel på en playbook som använder fakta:

```yaml
---
- name: Install Nginx
  hosts: web
  tasks:
    - name: Determine the package name
      set_fact:
        nginx_package: "{{ 'nginx' if ansible_facts['ansible_os_family'] == 'RedHat' else 'nginx-common' }}"
    - name: Install Nginx
      package:
        name: "{{ nginx_package }}"
        state: present
    - name: Start Nginx
      systemd:
        name: nginx
        enabled: yes
        state: started
```

I detta exempel använder vi fakta som samlas in av Ansible, speciellt `ansible_facts['ansible_os_family']`, för att avgöra vilket paket som ska installeras baserat på operativsystemsfamilj. På så sätt kan playbooken anpassa sig till olika typer av Linux-distributioner.

## Uppgift

Er uppgift blir att skapa en Ansible playbook som automatiserar följande uppgifter på en målenhet (web):

Installation av en webbserver. Om `ansible_os_family` är "RedHat" skall du installera Apache och mod_auth_gssapi, annars skall du installera webbservern Nginx. Webbservern skall därefter startas.

[Lösning](solutions/ansible_s4.md)

Föregående: [YAML Strukturerad Datahantering](ansible_s3.md)

Nästa: [Hantera inventariefiler i komplexa miljöer](ansible_s5.md)

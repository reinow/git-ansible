```
all:
  children:
    web:
      hosts:
        web_server1:
          ansible_host: 192.168.1.1
        web_server2:
          ansible_host: 192.168.1.2
    db:
      hosts:
        db_server1:
          ansible_host: 192.168.1.3
        db_server2:
          ansible_host: 192.168.1.4

```

```
all:
  children:
    web:
      hosts:
        web_server5:
          ansible_host: 192.168.1.9
        web_server6:
          ansible_host: 192.168.1.10
    db:
      hosts:
        db_server5:
          ansible_host: 192.168.1.11
        db_server6:
          ansible_host: 192.168.1.12
```

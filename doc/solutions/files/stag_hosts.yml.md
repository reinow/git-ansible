```
all:
  children:
    web:
      hosts:
        web_server3:
          ansible_host: 192.168.1.5
        web_server4:
          ansible_host: 192.168.1.6
    db:
      hosts:
        db_server3:
          ansible_host: 192.168.1.7
        db_server4:
          ansible_host: 192.168.1.8
```

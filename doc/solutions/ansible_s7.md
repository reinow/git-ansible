## Lektion 7

## Lektion 7

```
nginx_vhost/
├── tasks/
│   └── main.yml
├── templates/
│   └── nginx_vhost.j2
├── defaults/
│   └── main.yml
└── meta/
    └── main.yml
```

defaults/main.yml
```
server_name: example.com
root: /var/www/example.com
access_log: /var/log/nginx/example.com_access.log
error_log: /var/log/nginx/example.com_error.log
```

tasks/main.yml
```
- name: Install Nginx
  dnf:
    name: nginx
    state: present
  become: yes

- name: Copy virtual host configuration template
  template:
    src: nginx_vhost.j2
    dest: /etc/nginx/conf.d/example.conf
  notify:
    - Reload Nginx
```

templates/nginx_vhost.j2
```
server {
    listen 80;
    server_name {{ server_name }};
    root {{ root }};

    index index.html;

    access_log {{ access_log }};
    error_log {{ error_log }};

    location / {
        try_files $uri $uri/ =404;;
    }
}
```

playbook
```
- name: Configure Nginx
  hosts: web
  become: yes
  roles:
    - nginx_vhost

```

Lektion 7: [Hantera roller och mallar i Ansible](../ansible_s7.md)


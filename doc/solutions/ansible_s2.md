## Lektion 2

```
[all]
web.example.com
db.example.com

[web]
web.example.com

[db]
db.example.com
```
```
ssh-keygen
ssh-copy-id username@target_machine_ip
ssh username@target_machine_ip
```

Lektion 2: [Konfigurera Ansible](../ansible_s2.md)


## Lektion 4

```
---
- name: Installation of a Web Server
  hosts: webservers
  tasks:
    - name: Determine the web server package
      set_fact:
        web_server_package: "{{ 'httpd' if ansible_facts['ansible_os_family'] == 'RedHat' else 'nginx' }}"
    
    - name: Install the Web Server
      package:
        name: "{{ web_server_package }}"
        state: present

    - name: Start the Web Server
      systemd:
        name: "{{ 'httpd' if ansible_facts['ansible_os_family'] == 'RedHat' else 'nginx' }}"
        enabled: yes
        state: started

    - name: Install mod_auth_gssapi (for RedHat systems)
      package:
        name: mod_auth_gssapi
        state: present
      when: ansible_facts['ansible_os_family'] == 'RedHat'
```

Lektion 4: [Ansible Playbooks och Automatisering](../ansible_s4.md)


## Lektion 5

```
inventories
└── environment
    ├── development
    │   ├── group_vars
    │   │   ├── all
    │   │   │   └── all_vars.yml
    │   │   ├── db
    │   │   │   └── db_vars.yml
    │   │   └── web
    │   │       └── web_vars.yml
    ├── production
    │   ├── group_vars
    │   │   ├── all
    │   │   │   └── all_vars.yml
    │   │   ├── db
    │   │   │   └── db_vars.yml
    │   │   └── web
    │   │       └── web_vars.yml
    └── staging
        ├── group_vars
        │   ├── all
        │   │   └── all_vars.yml
        │   ├── db
        │   │   └── db_vars.yml
        │   └── web
        │       └── web_vars.yml
        └── hosts.yml
```

#### Development

- [db_vars.yml](files/db_vars.yml.md)
- [web_vars.yml](files/web_vars.yml.md)
- [hosts.yml](files/dev_hosts.yml.md)

#### Staging

- [db_vars.yml](files/db_vars.yml.md)
- [web_vars.yml](files/web_vars.yml.md)
- [hosts.yml](files/stag_hosts.yml.md)

#### Production

- [db_vars.yml](files/db_vars.yml.md)
- [web_vars.yml](files/web_vars.yml.md)
- [hosts.yml](files/prod_hosts.yml.md)

Lektion 5: [Hantera inventariefiler i komplexa miljöer](../ansible_s5.md)


## Lektion 3

```
- name: John Doe
  address:
    street: 123 Main St
    city: Anytown
    zip: 123 45
  phone:
    home: 0470 12 34 567
    work: 0470 98 76 543
  email: john.doe@email.com

- name: Jane Smith
  address:
    street: 456 Elm St
    city: Another Town
    zip: 543 21
  phone:
    mobile: 0708 12 34 56
  email: jane.smith@email.com

- name: Bob Johnson
  address:
    street: 789 Oak St
    city: Smallville
    zip: 678 90
  phone:
    home: 045 98 76 54
    mobile: 0709 32 16 789
  email: bob.johnson@email.com
```

Lektion 3: [YAML Strukturerad Datahantering](../ansible_s3.md)


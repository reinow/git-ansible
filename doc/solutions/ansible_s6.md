## Lektion 6

```
---
- name: Install MariaDB packages on DB servers
  hosts: db
  become: yes
  tasks:
    - name: Install MariaDB packages
      package:
        name: "{{ item }}"
        state: present
      loop:
        - mariadb
        - mariadb-backup
        - mariadb-server-utils

```

```
ansible-playbook -i inventories/environment/development/hosts.yml mariadb.yml
```

Lektion 6: [Arbeta med moduler i Ansible](../ansible_s6.md)


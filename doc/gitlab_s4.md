# Lektion 4

# Samarbetsbaserad utveckling med GitLab

I den här lektionen studerar vi samarbetsbaserad utveckling med GitLab. Vi utforskar hur du effektivt kan samarbeta med andra utvecklare, spåra ändringar och hantera eventuella konflikter i ett gemensamt projekt.

## Översikt över kodförråd och fjärranslutningar

Innan vi fördjupar oss i samarbetsaspekterna av GitLab, låt oss förstå några viktiga begrepp: kodförråd (repository) och fjärranslutningar.

### Kodförråd

Ett kodförråd är en central lagringsplats för ett projekt. Kodförrådet finns på en fjärrserver, till exempel på GitLab.com eller en egen GitLab-instans. Det är från detta kodförråd som du och andra utvecklare hämtar och delar projektets kod.

För att visualisera det, tänk på kodförrådet som den officiella platsen där ditt projekt lever. Alla ändringar och uppdateringar som du och ditt team gör kommer att återspeglas här.

### Fjärranslutningar

För att arbeta med kodförrådet behöver du upprätta fjärranslutningar. Dessa anslutningar gör det möjligt att interagera med kodförrådet från din lokala dator.

Det finns tre viktiga begrepp att komma ihåg när det gäller fjärranslutningar:

- **Hämta (Fetch):** Att hämta innebär att du laddar ner ändringar från kodförrådet till din lokala dator. Detta är användbart för att se de senaste ändringarna utan att påverka dina lokalt lagrade filer. Om det är önskvärt kan du efter att du har hämtat ändringar använda kommandot `git merge` för att uppdatera din lokala kod med de hämtade ändringarna.

```shell
# Fetch updates from the remote repository (origin)
git fetch
```

- **Pusha (Push):** Att pusha innebär att du laddar upp dina lokala ändringar till kodförrådet. På så sätt blir kodförrådet uppdaterat med dina senaste ändringar, och ditt arbete blir tillgängligt för andra.

- **Pulla (Pull):** Att pulla innebär att du hämtar ändringar från kodförrådet och sammanför dem med din nuvarande gren på din lokala dator. På så sätt kan du använda de senaste ändringarna från kodförrådet i ditt arbete.

## Arbeta med flera utvecklare inom ett gemensamt projekt

Nu när vi har en grundläggande förståelse för kodförråd och fjärranslutningar, låt oss utforska hur vi kan samarbeta med andra utvecklare inom ett gemensamt projekt.

### Dela projektet med andra

För att börja samarbeta med andra utvecklare måste du dela ditt projekt med dem. Detta innebär att du ger dem åtkomst till kodförrådet så att de kan hämta och arbeta med koden.

När det gäller användarbehörigheter erbjuder GitLab ett webbgränssnitt för hantering av projektåtkomst och behörigheter, vilket ofta är mer användarvänligt än att använda GitLabs API, särskilt för detaljerad kontroll över vem som kan göra vad i ditt projekt. Men många grundläggande uppgifter relaterade till behörigheter kan också utföras från kommandoraden med hjälp av `curl` och GitLabs API. För mer komplex hantering av behörigheter, som att konfigurera specifika åtkomstnivåer (t.ex. läs, skriv, underhåll, administratör) för enskilda användare eller grupper, är det dock ofta mer praktiskt att använda det webbgränssnitt som GitLab tillhandahåller. Detta ger projektadministratörer en central översikt och kontroll över åtkomsten.

På GitLab kan du enkelt dela ditt projekt med andra användare och grupper genom att gå till projektets sida och använda fliken "Manage" eller "Hantera". Här kan du bjuda in andra utvecklare via deras användarnamn eller e-postadresser. Du kan också bjuda en grupper. Du kan också kontrollera vilka behörigheter varje användare eller grupp har, som läsrättigheter eller skrivrättigheter.

Grupper måste först skapas innan de kan läggas till. En ny grupp skapas genom att klicka på GitLab ikonen längs uppe till vänster. Välj därefter menyalternativet "Groups" eller "Grupper".

När du skapar ett projekt under en grupp i GitLab tilldelas alla medlemmar i den gruppen automatiskt åtkomst till projektet som standard. Det innebär att gruppens medlemmar har samma nivå av åtkomst till projektet som de har till gruppen, såvida du inte uttryckligen begränsar eller ändrar deras åtkomst på projektnivå.

När andra utvecklare har blivit inbjudna och, i förekommande fall accepterat din inbjudan, kan de hämta projektet till sina lokala datorer och börja arbeta med projektet.

### Arbeta parallellt med grenar

När flera utvecklare arbetar på samma projekt samtidigt är det viktigt att hålla koll på ändringarna och undvika konflikter. En effektiv metod för detta är att använda grenar (branches) i Git.

Grenar gör det möjligt för varje utvecklare att arbeta isolerat med olika funktioner eller ändringar i projektet. På så sätt kan de utveckla sina funktioner utan att påverka huvudgrenen (vanligtvis kallad "main" eller "master") tills de är klara och testade.

För att skapa och hantera grenar på GitLab kan du använda kommandon som `git branch` och `git checkout`. Du kan byta mellan olika grenar och sammanfoga ändringar när de är redo.

### Hantera konflikter

I ett samarbetsprojekt är det nästan oundvikligt att konflikter uppstår när två utvecklare ändrar samma fil samtidigt. Git har inbyggda mekanismer för att hantera konflikter, men det kräver att utvecklarna kommunicerar och samarbetar.

När det uppstår en konflikt måste utvecklarna kommunicera för att förstå varför konflikten uppstod och hur den skall lösas. De kan använda kommandon som `git status` för att se vilka filer som är konflikter, och `git diff` för att visa de exakta ändringarna som orsakade konflikten.

Efter att konflikten är löst kan utvecklarna göra en ny commit och pusha ändringarna till kodförrådet för att dela med sig av sina lösningar.

### Användning av GitLab's Issue Tracker

GitLab's issue tracker är en kraftfull funktion som hjälper dig att effektivt hantera arbetsuppgifter och felrapporter inom ditt projekt. Med issue tracker kan du enkelt skapa, följa och prioritera arbetsuppgifter och problem.

#### Skapa en Issue

1. Gå till ditt projekt på GitLab och klicka på fliken "Issues" eller "Ärenden" i projektets meny.

2. Klicka på knappen "New Issue" eller "Nytt ärende" för att skapa en ny arbetsuppgift.

3. Ange en beskrivande titel och en detaljerad beskrivning av arbetsuppgiften eller problemet. Du kan även använda markdown för att formatera din beskrivning och lägga till bilder eller länkar.

4. Tilldela arbetsuppgiften till en person eller grupp genom att använda alternativet "Assignees" eller "Tilldelade". Du kan också ange en milstolpe (milestone) om så önskas.

5. Välj rätt "Label" eller "Etikett" för att kategorisera arbetsuppgiften. Exempel på etiketter kan vara "Bug," "Feature," eller "Enhancement."

6. Ange eventuell prioritet eller deadline för arbetsuppgiften.

7. Klicka på "Create issue" eller "Skapa ärende" för att skapa arbetsuppgiften.

#### Arbete med Issues

När arbetsuppgifter har skapats kan du och ditt team enkelt samarbeta kring dem:

- **Tilldela arbetsuppgifter:** Använd "Assignees" eller "Tilldelade" för att ange vem som är ansvarig för varje arbetsuppgift.

- **Kommentera och diskutera:** Du kan kommentera arbetsuppgifter för att diskutera detaljer eller ställa frågor.

- **Följa upp ändringar:** När du utför ändringar i koden kan du koppla ändringarna till specifika issues genom att inkludera issue-ID:n i dina commit-meddelanden. Till exempel, "Fixes #3" kommer att koppla commiten till issue nummer 3 och automatiskt stänga det när commiten är sammanfogad (merged) till huvudgrenen.

- **Prioritera och filtrera:** Använd olika filter och etiketter för att prioritera och organisera dina arbetsuppgifter. Du kan också använda milstolpar för att gruppera arbetsuppgifter som hör ihop.

Issue tracker är en ovärderlig resurs för att hantera projektarbete och säkerställa att inga arbetsuppgifter eller problem glöms bort.

### Uppgift

Som avslutning på denna lektionen kommer vi att öva på samarbetsbaserad utveckling med hjälp av GitLab för att skapa och hantera nätverkskonfigurationsfiler för domänen "mydomain.com".

1. **Skapa en grupp:** Inom varje arbetsgrupp, skapa en ny grupp på GitLab. Lägg till alla medlemmarna i er arbetsgrupp till denna grupp och ange "Software Developer" som gruppens roll i GitLab. En av gruppens medlemmar skall ansvara för IP-adresserna 192.168.1.0/24, en annan gruppmedlem skall ansvara för IP-adresserna 192.168.2.0/24 och en tredje medlem skall ansvara IP-adresserna 192.168.3.0/24.

2. **Skapa ett projekt:** Varje användare i arbetsgruppen skapar ett nytt projekt under denna grupp. Välj gruppen som du skapade tidigare när du skapar projektet. Namnge projekten: "hosts", "zone" och "reverse-zone".

3. **Projekt 1 - Hosts-fil:** Ägaren till det första projektet fokuserar på att skapa och underhålla en hosts-fil för nätverkskonfiguration, specifikt i Linux-system (/etc/hosts). Kom ihåg att inkludera en README.md-fil för projektet som beskriver dess syfte, mål och hur man bidrar till projektet. Filen hosts innehåller uppgifter om namn och IP-adress till datorer på ett nätverk. Filen hosts används främst för att vissa datorer skall hittas innan DNS-systemet blivit funktionellt vid uppstart eller som en "backup" mot störningar på nätverket.

4. **Projekt 2 - Zone-fil för Named:** Ägaren till det andra projektet fokuserar på att skapa och underhålla en zone-fil för Named (BIND DNS server). Filen används för att definiera DNS-zoner och deras resursposter. Inkludera en README.md-fil som beskriver projektets syfte, mål och hur man bidrar till projektet.

5. **Projekt 3 - Reverse Zone-fil för Named:** Ägaren till det tredje projektet fokuserar på att skapa och underhålla en reverse zone-fil för Named (BIND DNS server). Filen används för att hantera omvänd DNS-uppslagning. Lägg till en README.md-fil som beskriver projektets syfte, mål och hur man bidrar till projektet.

6. **Arbeta med grenar:** Alla medlemmar i arbetsgruppen skall klona alla projekten till sina lokala datorer och agera som, i förekommande fall, projektägare eller utvecklare. Datorerna inom ett fiktivt nätverk och deras IP-adresser skall läggas till både i hosts-filen och de båda zon-filerna. Prova att skapa nya grenar, utföra ändringar och samarbeta genom att göra sammanfogningar (merging) och försöka lösa eventuella konflikter.

7. **Använd GitLab's issue tracker:** I projektet, använd GitLab's issue tracker för att skapa och hantera arbetsuppgifter och felrapporter. Skapa minst två olika typer av issues, till exempel "Feature Request" och "Bug Report." Tilldela dessa issues till olika gruppmedlemmar och använd issue tracker för att kommunicera.

8. **Lös en konflikt:** Om du känner dig bekväm, provocera fram en konflikt genom att ändra samma fil i olika grenar. Försök sedan lösa konflikten genom att använda GitLab's verktyg för konflikthantering.

Föregående: [Grunderna i Git](gitlab_s3.md)

Nästa: [Nästa Lektion: Avancerade funktioner i GitLab: Grenstrategier](gitlab_s5.md)


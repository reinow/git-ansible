# Lektion 5

# Hantera inventariefiler i komplexa miljöer

I den här lektionen studerar vi inventariefiler för att definiera dina målenheter i komplexa miljöer. Vi studerar hur du effektivt hanterar grupper och variabler i inventariefilerna, likson olika driftsmiljöer.

## Förstå inventariefiler och deras betydelse i Ansible

Inventariefiler är en grundläggande komponent i Ansible. De fungerar som en katalog med data om de målenheter som ska hanteras. Dessa filer innehåller en lista över målenheter som Ansible kommer att interagera med. Inventariefiler är nödvändiga för att strukturera och organisera dina automatiseringsuppgifter.

Ansible kan arbeta med inventariefiler i olika format, inklusive INI- och YAML-format. Du kan välja det format som passar bäst för ditt projekt och din arbetsprocess. Inventariefiler kan vara lokala eller tllhandahållas över nätverket för att passa olika scenarier.

Du kan använda separata inventariefiler för olika miljöer (till exempel production, staging, development).

## Skapa och konfigurera inventariefiler för att definiera målenheter

För att skapa och konfigurera inventariefiler behöver du först förstå dess syntax och struktur. Låt oss ta en titt på några grundläggande aspekter av inventariefiler:

- **Målenheter (Hosts)**: Målenheter representerar de servrar eller enheter som Ansible kommer att hantera. Du kan lista målenheter en efter en i inventariefilen.

- **Grupper (Groups)**: För att organisera och hantera dina målenheter kan du skapa grupper. Grupper gör det möjligt att hantera flera målenheter som en enhet. Till exempel kan du ha en grupp för webbservrar och en annan för databasservrar.

- **Variabler (Variables)**: Inventariefiler kan också innehålla variabler som är specifika för målenheter eller grupper. Dessa variabler kan användas för att konfigurera uppgifter som SSH-portar, användarnamn eller lösenord.

### Exempel på inventariefil i INI-format:

```ini
[webservers]
webserver1 ansible_host=192.168.1.101 ansible_user=admin ansible_ssh_pass=yourpassword

[databases]
dbserver1 ansible_host=192.168.1.102 ansible_user=dbadmin ansible_ssh_pass=yourpassword
```

### Exempel på inventariefil i YAML-format:

```yaml
all:
  web:
    hosts:
      webserver1:
        ansible_host: 192.168.1.101
        ansible_user: admin
        ansible_ssh_pass: yourpassword
  db:
    hosts:
      dbserver1:
        ansible_host: 192.168.1.102
        ansible_user: dbadmin
        ansible_ssh_pass: yourpassword
```

Du kan också använda `children` för att organisera målenheter i grupper. Denna struktur förtydligar hierarki och gruppering. Grupperna `web` och `db` är undergrupper till den övergripande gruppen `all`. Denna struktur kan underlätta hanteringen av inventariefiler, särskilt i större och mer komplexa miljöer.

```yaml
all:
  children:
    web:
      hosts:
        webserver1:
          ansible_host: 192.168.1.101
          ansible_user: admin
          ansible_ssh_pass: yourpassword
    db:
      hosts:
        dbserver1:
          ansible_host: 192.168.1.102
          ansible_user: dbadmin
          ansible_ssh_pass: yourpassword
```

## Hantera grupper och variabler i inventariefiler

Att organisera målenheter och grupper är viktigt för att hålla inventariefiler strukturerade och överskådliga. Du kan använda grupper för att samla liknande målenheter och använda variabler för att konfigurera dem.

### Exempel på att använda grupper och variabler i inventariefiler:

```yaml
all:
  children:
    web:
      hosts:
        webserver1:
          ansible_host: 192.168.1.101
          ansible_user: admin
        webserver2:
          ansible_host: 192.168.1.102
          ansible_user: admin
    db:
      hosts:
        dbserver1:
          ansible_host: 192.168.1.201
          ansible_user: dbadmin
          db_port: 5432
        dbserver2:
          ansible_host: 192.168.1.202
          ansible_user: dbadmin
          db_port: 5432
```

I detta exempel har vi skapat två grupper, `web` och `db`, och tilldelat variabler som `db_port` för att konfigurera målenheter inom gruppen `db`. På detta vis blir det enkelt att hantera och konfigurera målenheter och grupper med liknande egenskaper.

Genom att förstå hur du skapar och hanterar inventariefiler samt organiserar målenheter och grupper i inventariefilerna, kan du effektivisera och strukturera dina Ansible-automatiseringsuppgifter.

## Val av Format i Inventariefiler

I inventariefiler i YAML-format kan du använda antingen dictionary (nyckel-värde par) eller listor för att definiera målenheter med tillhörande attribut.

### Dictionary-Format (Nyckel-Värde Par)

I detta format definierar du varje målenhet och dess attribut som nyckel-värde par. Det är ett mer explicit sätt att definiera målenheternas variabler, med god läsbarhet.

Exempel:

```yaml
all:
  hosts:
    web_server1:
      ansible_host: 192.168.1.1
      ansible_user: admin
    web_server2:
      ansible_host: 192.168.1.2
      ansible_user: admin
```

### List-Format (-)

I detta format definierar du varje målenhet och dess attribut som element i en lista. Detta format kan vara mer koncist när du har många målenheter med liknande attribut, vilket kan göra det lättare att hantera. Varje målenhet och dess attribut definieras som ett dictionary inom listan.

Exempel:

```yaml
all:
  hosts:
    - name: web_server1
      ansible_host: 192.168.1.1
      ansible_user: admin
    - name: web_server2
      ansible_host: 192.168.1.2
      ansible_user: admin
```

Valet mellan dessa format beror i hög grad på dina preferenser och komplexiteten i din miljö. Listor används oftast när du har många målenheter med liknande attribut för att undvika upprepning av samma nycklar. Dictionaries används ofta när du behöver tilldela olika typer av attribut till varje målenhet.

Båda formaten är giltiga med fullt stöd i Ansible. Välj vad som bäst passar dina behov och gör din inventariefil mer läsbar och lättare att underhålla.

## Driftsmiljöer

Även om Ansible erbjuder lättläst syntax, flexibla arbetsflöden och kraftfulla verktyg kan det vara utmanande att hantera ett stort antal målenheter med olika driftsmiljö och funktionalitet. Till exempel kan minneskraven för en utvecklingsserver vara mindre än en server i produktionsmiljö. Det är därför viktigt att skapa en bra struktur som omfattar både driftsmiljö och funktionalitet.

Ansible-projektet tillhandahåller rekommendationer hur du bäst kan abstrahera din infrastruktur över olika miljöer. 
Det rekommenderade tillvägagångssättet är att separera varje driftsmiljö. Istället för att hantera alla dina målenheter inom en enda inventariefil, rekommenderas en separat inventariefil för var och en av dina miljöer.

Exemplet nedan visar hur katalogstrukturen, enligt Ansibles rekommendationer, kan se ut med grupperna "web" och "db".

```
inventories
└── environment                      # Parent directory for the environment-specific directories
    ├── development                  # Contains all files specific to the development environment
    │   ├── group_vars               # development specific group_vars files
    │   │   ├── all
    │   │   │   └── all_vars.yml     # All environment-wide variables for development
    │   │   ├── db
    │   │   │   └── db_vars.yml      # Database server specific variables for development
    │   │   └── web
    │   │       └── web_vars.yml     # Web server specific variables for development
    │   └── hosts.yml                # Contains only the hosts in the development environment
    ├── production                   # Contains all files specific to the production environment
    │   ├── group_vars               # production specific group_vars files
    │   │   ├── all
    │   │   │   └── all_vars.yml     # All environment-wide variables for production
    │   │   ├── db
    │   │   │   └── db_vars.yml      # Database server specific variables for production
    │   │   └── web
    │   │       └── web_vars.yml     # Web server specific variables for production
    │   └── hosts.yml                # Contains only the hosts in the production environment
    └── staging                      # Contains all files specific to the staging environment
        ├── group_vars               # staging specific group_vars files
        │   ├── all
        │   │   └── all_vars.yml     # All environment-wide variables for staging
        │   ├── db
        │   │   └── db_vars.yml      # Database server specific variables for staging
        │   └── web
        │       └── web_vars.yml     # Web server specific variables for staging
        └── hosts.yml                # Contains only the hosts in the staging environment

```

## Uppgift

Skapa en katalogstruktur genom att köra scriptet nedan från projektets rotkatalog. Glöm inte att tilldela scriptet "execute" rättigheter.

```bash
#!/bin/bash

mkdir -p inventories/environment
pushd inventories/environment > /dev/null

for d in development staging production; do
  mkdir -p "$d/group_vars"
  for sd in all db web; do
    mkdir -p "$d/group_vars/$sd"
    echo "# Variables specific to the $sd group" > "$d/group_vars/$sd/${sd}_vars.yml"
  done
  echo "# Hosts specific to the $d environment" > "$d/hosts.yml"
done

popd > /dev/null
```

Skapa en inventariefil i YAML-format för varje driftsmiljö: "development", "staging" och "production". Inom varje miljö finns både webb- och databasservrar. Webbservrarna tillhör gruppen "web" och databasservrarna tillhör gruppen "db". Använd uppgifterna nedan för målenheterna.

#### Driftsmiljö

- **Development**
  - `web_server1: 192.168.1.1`
  - `web_server2: 192.168.1.2`
  - `db_server1: 192.168.1.3`
  - `db_server2: 192.168.1.4`
- **Staging**
  - `web_server3: 192.168.1.5`
  - `web_server4: 192.168.1.6`
  - `db_server3: 192.168.1.7`
  - `db_server4: 192.168.1.8`
- **Production**
  - `web_server5: 192.168.1.9`
  - `web_server6: 192.168.1.10`
  - `db_server5: 192.168.1.11`
  - `db_server6: 192.168.1.12`

#### Gruppvariabler

- **web**
  - `http_port: 80`
  - `https_port: 443`
- **db**
  - `db_port: 3306`

---

[Lösning](solutions/ansible_s5.md)

Föregående: [Ansible Playbooks och Automatisering](ansible_s4.md)

Nästa: [Arbeta med moduler i Ansible](ansible_s6.md)


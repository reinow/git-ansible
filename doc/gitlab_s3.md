# Lektion 3

# Grunderna i Git

I den här lektionen studerar vi de grundläggande koncepten i Git. Vi kommer att lära oss hur Git-arbetsflödet fungerar, inklusive commits, grenar (branches), sammanfogningar (merges), samt hur man utför grundläggande Git-operationer som push och pull. De kommandon some anges i texten är avsedda att utföras från root-biblioteket i Git-projektet i terminalen.

### Commits
En commit är en central del av arbetsflödet i Git. En commit representerar en ögonblicksbild (snapshot) av ditt projekt vid en viss tidpunkt. Det är ett sätt att spara ändringar och göra dem spårbara.

Om du har gjort ändringar i ditt projekt, kan du utföra en commit för att spara dessa ändringar och ange en beskrivning av vad som har ändrats. Det skapas härmed en punkt i historiken som du senare kan återvända till om det behövs.

I Git används kommandot `git commit` för att skapa en commit. Här är de grundläggande stegen:

```shell
# Lägg till ändringar
git add filnamn

# Utför commit med en beskrivning
git commit -m "Din beskrivning"
```
Kommandot `git add` är vanligt förekommande när du arbeter med Git. Kommandot genomför förändringar i din arbetskatalog och gör den redo för en commit. Du väljer de filer eller kataloger du vill inkludera i din nästa commit. Du kan använda `git add` för att lägga till enskilda filer, flera filer eller till och med hela kataloger. Du kan också använda wildcards, med tecknet *, för att välja flera filer på en gång. Efter att du lagt till filerna kan du köra `git commit`.

Om du har gjort en commit och vill ta bort den innan du delar den med andra, kan du använda kommandot `git reset`. Här beskrivs momenten du behöver utföra:

Först, använd kommandot `git log` för att visa din commit-historik och identifiera den sista commiten du vill behålla. Kommandot `git log` kommer att visa dig en lista ned commit-hashar och information för varje commit.

```shell
git log
```

När du har identifierat commiten, kan du använda kommandot `git reset` med flaggan --hard och ange commit-hashen för den sista commiten du vill behålla. Till exempel:

```shell
git reset --hard f43d0fb78a173bd799eb59ab0315be9ad571a180
```

Man kan också använda begreppet HEAD, som vanligtvis refererar till den senaste commiten. Till exempel, om du vill ta bort den senaste commiten, använd:

```shell
git reset --hard HEAD~1
```

Detta kommer att ångra den senaste commiten och återställa till tillståndet innan du gjorde denna commit. Termen 'HEAD' i Git är en speciell pekare, eller referens, till den senaste commiten i den aktuella grenen. HEAD är ett grundläggande koncept i Git som används för att representera den senaste commiten i grenen du för närvarande arbetar med.

### Grenar (Branches)

Grenar är en kraftfull funktion i Git som gör det möjligt att arbeta med olika delar av ett projekt samtidigt. Grenarna används för att isolera ändringar så att de inte påverkar huvudgrenen (vanligtvis kallad "main" eller "master") tills de är färdiga och testade.

**Exempel:** Anta att du arbetar på en webbplats och vill lägga till en ny funktion, exempelvis en inloggingsfunktion, utan att riskera att uppdatera den befintliga koden. Du kan skapa en ny gren, göra dina ändringar där, och när de är klara och testade, kan du sammanfoga grenen med huvudgrenen.

För att hantera grenar i Git, använd följande kommandon:

- Skapa en ny gren:
```shell
git branch ny-gren
```
- Byt till en befintlig gren:
```shell
git checkout ny-gren
```
- Skapa och byt till en ny gren i ett steg:
```shell
git checkout -b ny-gren
```
- Lista alla grenar i ditt projekt: `git branch`
```shell
git branch
```
- Ta bort en gren: `git branch -d gren-att-ta-bort`
```shell
git branch -d gren-att-ta-bort
```

### Sammanfogningar (Merges)

Git kombinerar ändringar genom sammanfogningar av olika grenar till en gemensam gren. När du är nöjd med ändringarna i en gren och vill inkludera dem i huvudgrenen eller en annan gren, sammanfogar du grenarna.

**Exempel:** Om du har skapat en gren för att lägga till en ny funktion och testat den noggrant, vill du förmodligen inkludera dessa ändringar i huvudgrenen. Du kan göra detta genom att utföra en sammanfogning.

För att utföra en enkel sammanfogning, använd kommandot `git merge`. Här visas de grundläggande stegen:

```shell 
# Byt till målgrenen
git checkout main

# Utför sammanfogningen med grenen ny-funktion
git merge ny-funktion
```

### Push och Pull

För att ladda upp dina ändringar till fjärrkodförrådet och dela ditt arbete med andra använder du kommandot `git push`. För att hämta ändringar från fjärrkodförrådet (remote repository), använder du kommandot  `git pull`. Dessa kommandon är verktygen i samarbetet med andra utvecklare och för att hålla ditt arbete synkroniserat med det gemensamma projektet. I många fall behöver man inte ange några ytterigare parametrar till `git push` och `git pull`.

#### Git Push

När du använder `git push`, skickar du dina lokala commits till fjärrkodförrådet. Detta innebär att du delar dina ändringar och gör dem tillgängliga för andra som samarbetar i samma projekt.

   ```shell
   git push
   ```

Här är några användbara sätt att använda `git push`:

- **`git push origin`**: Detta kommando används för att skickas dina commits till det fjärrkodförråd som du ursprungligen klonade:

   ```shell
   git push origin
   ```

- **`git push origin branch-name`**: Detta kommando används för att skicka dina commits från en specifik lokal gren till fjärrkodförrådet under samma namn. Till exempel, om du arbetar på en gren som heter "feature" och vill skicka dina ändringar till det gemensamma fjärrkodförrådet:

   ```shell
   git push origin feature
   ```

- **`git push origin from-branch:to-branch`**: Detta kommando används för att skicka dina commits från en specifik lokal gren till en annan specifik gren i fjärrkodförrådet. Till exempel, om du arbetar på en gren som heter "recent" och vill skicka dina ändringar till grenen "future" i det gemensamma fjärrkodförrådet:

   ```shell
   git push origin recent:future
   ```

- **`git push --all origin`**: Om du har flera grenar och vill skicka alla till fjärrkodförrådet, kan du använda detta kommando. Det kommer att skicka alla lokala grenar till fjärrkodförrådet "origin".

   ```shell
   git push --all origin
   ```

#### Git Pull

`git pull` är ett annat viktigt kommando och används för att hämta ändringar från fjärrkodförrådet och uppdatera din lokala kopia med de dessa. `git pull` består faktiskt av två steg: `git fetch` och `git merge`. Här beskrivs hur du använder `git pull`:

- **`git pull origin branch-name`**: När du kör detta kommando, hämtar Git de senaste ändringarna från den angivna grenen på fjärrkodförrådet "origin" och sammanfogar dem automatiskt med din nuvarande lokala gren. Det är en praktiskt sätt att se till att du arbetar med den senaste koden från fjärrkodförrådet.

   ```shell
   git pull origin feature
   ```

#### Parametrar (Options)

Båda `git push` och `git pull` kommandon har ett antal parametrar som du kan använda dig av. Här är några exempel:

- **`-f` (force)**: Används för att tvinga push- eller pull-operationen även om det finns konflikter. Detta kommando bör användas med försiktighet.

   ```shell
   git push -f origin branch-name
   ```

- **`--dry-run`**: Med detta alternativ kan du simulera en push- eller pull-operation för att se vilka ändringar som kommer att utföras, utan att genomföra dem.

   ```shell
   git pull --dry-run origin branch-name
   ```

- **`-v` (verbose)**: Ger detaljerade utskrifter om vad som händer under push- eller pull-processen. Det kan vara användbart för felsökning.

Nu har vi en bättre förståelse för hur `git push` och `git pull` fungerar och hur de kan anpassas med hjälp av parametrar. `git push` och `git pull` är viktiga kommandon i Git och kommer att vara oumbärliga i ditt arbetsflöde när du samarbetar med andra utvecklare.

### Uppgift

För att fördjupa din förståelse, skall du utföra följande uppgifter. Dessa uppgifter kräver att du använder olika Git-operationer, inklusive push, pull och sammanfogningar (merges) mellan olika grenar.

1. **Skapa ett privat projekt**: Använd GitLabs webbgränssnitt för att skapa ett nytt privat projekt med en README.md fil. Ange ett passande namn för projektet och se till att det är privat (inte tillgängligt för allmänheten), och vid behov konfigurera projektet.

2. **Visa och kopiera en fil**: Gå till följande webbadress: [https://gitlab.com/reinow/git-ansible/-/blob/main/code/flask.py?ref_type=heads](https://gitlab.com/reinow/git-ansible/-/blob/main/code/flask.py?ref_type=heads). Använd sedan RAW-formatet för att kopiera filens innehåll till urklipp.

3. **Lägg till filen i ditt projekt**: Använd GitLabs webbgränssnitt för att lägga till den tidigare kopierade "flask.py"-filen i det privata projekt du skapade under punkt 1.

4. **Klona projektet till din lokala dator**: Använd lämpligt Git-kommando för att klona projektet till din lokala dator.

5. **Skapa flera grenar**: Använd Git-kommandon på din lokala dator för att skapa tre nya grenar i ditt projekt. Namnge grenarna på ett sätt som beskriver deras syfte eller funktion, till exempel "ny-funktion", "experimentell-utveckling" och "korrigeringar".

6. **Arbeta i grenarna**: Byt mellan de olika grenarna och gör ändringar i filer som finns i projektet. Du kan lägga till, redigera eller ta bort innehåll. Utför någon form an förändring i de skapade grenarna.

7. **Push och pull mellan grenar**: Använd Git-kommandon för att utföra både push och pull mellan grenarna. Till exempel, försök att pusha ändringar från en gren till en annan, och sedan pulla dessa ändringar från den andra grenen.

8. **Utför en komplex sammanfogning (merge)**: Skapa en situation där två av dina grenar har ändringar som påverkar samma filer. Försök sedan att utföra en sammanfogning av de två grenarna till huvudgrenen. Lösa eventuella konflikter som uppstår under sammanfogningen.

9. **Observera GitLab-historiken**: Använd GitLabs webbgränsnitt för att utforska historiken, grenarna och ändringarn i ditt projekt. Försök förstå hur dina Git-operationer påverkar projektets historik och flöde.

Använd [GitLabs dokumentation](https://docs.gitlab.com/) om du behöver hjälp med specifika Git-kommandon. Lycka till med uppgiften!

Föregående: [Skapa och konfigurera ett GitLab-projekt](gitlab_s2.md)

Nästa: [Samarbetsbaserad utveckling med GitLab](gitlab_s4.md)

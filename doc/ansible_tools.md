1. **ansible-config (1)**: Detta kommando används för att visa Ansibles konfigurationsinställningar. Du kan använda det för att visa den aktuella konfigurationen och kontrollera vilka konfigurationsfiler som används.

2. **ansible-console (1)**: Ansible Console är en interaktiv shell för Ansible. Den ger ett bekvämt sätt att experimentera med Ansible-moduler och uppgifter i realtid. Du kan testa Ansible-uppgifter och moduler direkt från kommandoraden, vilket gör den till ett användbart verktyg för felsökning och inlärning.

3. **ansible-doc (1)**: Ansible Doc är ett verktyg för att visa dokumentation för Ansible-moduler. Det ger detaljerad information om Ansible-moduler, inklusive deras parametrar och användningsexempel. Detta verktyg är otroligt användbart när du behöver förstå hur du använder en specifik modul.

4. **ansible-galaxy (1)**: Ansible Galaxy är ett verktyg för att hantera Ansible-roller. Det låter dig söka, installera och hantera roller som skapats och delats av Ansible-samhället. Ansible Galaxy förenklar processen att återanvända och dela Ansible-automatiseringsinnehåll.

5. **ansible-inventory (1)**: Ansible Inventory är ett kommandoradsverktyg för att visa och hantera Ansibles inventering. Inventering i Ansible definierar de värdar och grupper av värdar på vilka Ansible-operationer kommer att köras. Detta verktyg hjälper dig att lista och manipulera inventeringsvärdar och grupper.

6. **ansible-playbook (1)**: Ansible Playbook är ett av de centrala verktygen i Ansible. Det låter dig definiera och köra Ansible-automatiseringsuppgifter i ett deklarativt YAML-format. Playbooks används för att beskriva önskat tillstånd för system och automatisera de uppgifter som behövs för att uppnå det tillståndet.

7. **ansible-pull (1)**: Ansible Pull är ett fristående skript som gör det möjligt att köra Ansible-playbooks på en målmiljö. Istället för att skicka konfigurationer från en kontrollnod "drar" du konfigurationerna från en versionshanterad miljö som Git. Det är användbart för att tillämpa konfigurationer på fjärrmaskiner utan att behöva ha en central kontrollnod.

8. **ansible-vault (1)**: Ansible Vault är ett verktyg för att kryptera känsliga data som används i Ansible-playbooks eller variabler. Det hjälper till att säkra känslig information som lösenord, API-nycklar och andra hemligheter. Du kan kryptera och dekryptera data med Ansible Vault och säkerställa att känslig information är skyddad.

Dessa verktyg tillsammans gör Ansible till ett kraftfullt och mångsidigt automatiseringsramverk för att hantera och konfigurera IT-infrastruktur. Varje verktyg tjänar ett specifikt syfte inom Ansibles arbetsflöde, vilket gör det enklare att automatisera uppgifter, hantera konfigurationer och hantera känslig information säkert.

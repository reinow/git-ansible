# Lektion 1

# Introduktion till versionshantering och GitLab

Välkommen till den första lektionen av utbildningen, "Kom igång med GitLab." Under denna lektion kommer vi att studera hur GitLab kan användas för både mjukvaruutveckling och skapande av infrastruktur som kod och plattform som kod.

## Förstå behovet av versionshanteringssystem

För att förstå varför versionshanteringssystem är så viktiga, låt oss ta en närmare titt på de utmaningar som både mjukvaruutvecklare, infrastrukturutvecklare och plattformsutvecklare möter. I ett ständigt föränderligt IT-landskap är det avgörande att bevara kontrollen över sin kod. Här är några av de viktigaste utmaningarna:

### För mjukvaruutvecklare:

- **Ändringshantering:** Mjukvaruprojekt utvecklas ständigt, och det kan vara en utmaning att spåra och hantera alla ändringar korrekt.

- **Samarbete:** Team arbetar ofta parallellt med olika funktioner eller buggfixar. Konflikter och problem kan uppstå när ändringar måste sammanfogas.

- **Säkerhetskopiering:** Förlust av källkod på grund av systemfel eller mänskliga misstag kan vara förödande. Att skapa säkra och pålitliga säkerhetskopior är avgörande.

### För plattformsutvecklare och infrastrukturutvecklare:

- **Ändringshantering:** Att skapa och hantera infrastruktur eller plattform som kod kräver en genomtänkt struktur. Ändringar i infrastrukturen eller plattformen måste vara spårbara och reproducerbara.

- **Samarbete:** Team som skapar infrastruktur och plattform behöver effektivt samarbeta, diskutera förändringar och säkerställa att kodstandarder och säkerhetskrav uppfylls.

- **Automatisering:** Att automatisera bygg-, test- och distributionsprocesser för infrastrukturen eller plattformen är viktigt för att öka effektiviteten och minimera risken för fel.

För att hantera dessa utmaningar används versionshanteringssystem (VCS) och verktyg som GitLab.

För den vetgirige kan nämnas att IaC handlar om att skapa och hantera infrastrukturella resurser som beräkningskapacitet, lagring och nätverk på infrastrukturnivån. Å andra sidan handlar PaC om plattformsnivån, inklusive operativsystem, bibliotek och utvecklingsverktyg.

## Översikt över distribuerad versionshantering och Git

Versionshanteringssystem kan delas in i två huvudtyper: centraliserade och distribuerade. Centraliserade system har en enda central server där projektets källkod och andra filer lagras, medan distribuerade system ger varje utvecklare en komplett kopia av hela projektet med dess historik. Git är ett exempel på ett distribuerat versionshanteringssystem och det är Git vi kommer att fokusera på under denna utbildning.

Git är utvecklat av Linus Torvalds, skaparen av Linux, och har blivit en branschstandard för versionshantering. Med sin förmåga att hantera projekt av alla storlekar och dess starka stöd för samarbete är Git ett oumbärligt verktyg för modern mjukvaruutveckling. Git är naturligtvis också av samma skäl mycket användbart för plattformsutvecklare och infrastukturutvecklare för att hantera plattform och infrastruktur som kod.

Låt oss se hur GitLab kan användas för mjukvaruutveckling, infrastruktur som kod och plattform som kod med några exempel:

### Kodförrådshantering:

- För mjukvaruutvecklare: GitLab gör det enkelt att skapa och hantera Git-kodförråd för olika typer av projekt. Till exempel kan man skapa ett nytt projekt för sin Java kod och ladda upp sin källkod från sin lokala dator.

- För plattformsutvecklare och infrastrukturutvecklare: GitLab kan användas för att hantera infrastruktur och plattsform som kod. Till exempel, om du hanterar en Kubernetes-kluster som kod, kan du lagra konfigurationsfiler i ett GitLab-kodförråd.

### Samarbete:

- För mjukvaruutvecklare: Eftersom GitLab är en webbplattform kan ditt mjukvaruteam samarbeta smidigt oavsett var de befinner sig. Du kan skapa ärenden för buggfixar eller nya funktioner, diskutera ändringar och genomföra kodgranskningar direkt i GitLab.

- För plattformsutvecklare och infrastrukturutvecklare: GitLab möjliggör samarbete för plattformsutvecklingsteam. Du kan enkelt hantera och diskutera infrastrukturrelaterade ärenden, granska ändringar i din infrastrukturkonfiguration och säkerställa att ditt team arbetar effektivt tillsammans.

### Integrerad CI/CD:

- För mjukvaruutvecklare: GitLab har inbyggt stöd för Continuous Integration (CI) och Continuous Deployment (CD), vilket automatiserar bygg- och distributionsprocessen för din mjukvara. Till exempel kan du ställa in GitLab CI/CD-pipelines för att automatiskt bygga, testa och distribuera din programvara när du gör ändringar i källkoden.

- För plattformsutvecklare och infrastrukturutvecklare: CI/CD är också viktigt vid plattformsutveckling och infrastrukturutveckling. GitLab CI/CD tillhandahåller funktioner för att automatisera tester och driftsättning av infrastruktur- eller plattformsförändringar. Till exempel kan du använda CI/CD-pipelines för att automatisera uppdateringen av ditt Kubernetes-kluster när du ändrar din infrastrukturkonfiguration.

### Säkerhet:

- För mjukvaruutvecklare: GitLab erbjuder kraftfulla säkerhetsfunktioner som hantering av användarrättigheter och kontroll av känslig information.

- För plattformsutvecklare och infrastrukturutvecklare: Säkerhet är en prioritet även vid plattformsutveckling och infrastrukturutveckling.

I denna första lektion har vi utforskat varför versionshantering är viktigt både för mjukvaruutveckling, infrastrukturutveckling och plattformsutveckling, hur distribuerad versionshantering med Git fungerar och hur GitLab hjälper dig att hantera kod.

## Uppgift
Skapa ett konto hos [GitLab](https://gitlab.com/users/sign_up) och uppdatera din profil.

## Ytterligare Resurser
För mer information och avancerade funktioner, fördjupa dig i [GitLab's officiella dokumentation](https://docs.gitlab.com/ee/).

Nästa: [Skapa och konfigurera ett GitLab-projekt](gitlab_s2.md)



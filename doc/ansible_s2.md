# Lektion 2

# Konfigurera Ansible

I den andra lektionen av vår Ansible kurs fördjupar vi oss i installationen och konfigurationen av Ansible. Det är naturligtvis viktigt att installation och konfiguration blir korrekt utförda. För att ange vilka målenheter som ska hanteras av Ansible skapar du en inventariefil, och för att säkerställa en säker anslutning till dina målenheter skapar du en SSH-anslutning.

## Installera Ansible på kontrollnoden

För att komma igång med Ansible måste du naturligtvis först installera det på din kontrollnod, den dator där du planerar att köra dina automatiseringsuppgifter. Ansible är tillgängligt för de flesta Linux-distributioner och kan enkelt installeras med hjälp av din distributions pakethanterare. Här är några exempel på hur du installerar Ansible på några vanliga Linux-distributioner:

**Ubuntu/Debian:**

```bash
sudo apt-get update
sudo apt-get install ansible
```

**Red Hat/CentOS/Fedora:**

```bash
sudo dnf install epel-release
sudo dnf install ansible
```

Efter installationen kan du verifiera att Ansible har installerats korrekt genom att köra följande kommando:

```bash
ansible --version
```

Detta kommer att visa den installerade Ansible-versionen och bekräftar att Ansible är redo att användas.

## Konfigurera Ansible-inventariet

Inventariet i Ansible definierar de målenheter som dina automatiseringsuppgifter kommer att riktas mot. Det kan vara servrar, virtuella maskiner eller andra enheter i din infrastruktur. Inventariet skapas vanligtvis som en enkel textfil och kan vara mycket enkel eller mycket komplex, beroende på din infrastrukturs storlek och komplexitet.

Här är ett grundläggande exempel på en inventariefil i INI-format:

```ini
[all]
server1.example.com
server2.example.com
webserver.example.com
db1.example.com
db2.example.com

[finance]
server1.example.com
server2.example.com

[web]
webserver.example.com

[databases]
db1.example.com
db2.example.com
```

I ovanstående exempel har vi fyra grupper: "all," "finance," "web," och "databases," och vi har listat målenheter under varje grupp. Du kan ange målenheter antingen med sina IP-adresser eller deras värdnamn. Inventariefilen lagras vanligtvis i katalogen /etc/ansible/ eller i en separat arbetskatalog.

När din inventarielista är konfigurerad kan du använda den i dina Ansible-playbooks och uppgifter (tasks) för att rikta automatiseringsåtgärder mot de definierade målenheterna.

Du kan kontrollera syntaxen för din inventariefil med följande kommando;

```bash
ansible-inventory -i /path/to/your/inventory/file --list
```

## Skapa SSH-anslutning till målenheter

Ansible använder SSH-protokollet för att kommunicera med målenheter. För att säkerställa att du kan ansluta till dina målenheter via SSH, måste du se till att SSH-anslutningen är upprättad och fungerar korrekt.

För att säkerställa en säker SSH-anslutning utan att du behöver ange användarnamn och löseneord genomför följande:

1. **Generera ett SSH-nyckelpar**: Om du inte redan har ett SSH-nyckelpar på din kontrollnod, kan du generera ett med följande kommando:

```bash
ssh-keygen
```

Du kan använda standardinställningarna eller konfigurera nyckeln enligt dina preferenser.

2. **Kopiera den publika nyckeln till målenheten**: Använd följande kommando för att kopiera din publika SSH-nyckel till målenheten:

```bash
ssh-copy-id username@target_machine_ip
```

Ersätt "username" med ditt användarnamn på målenheten och "target_machine_ip" med målenhetens IP-adress. Du kommer att bli ombedd att ange lösenordet för användarkontot på målenheten för att slutföra processen.

Testa SSH-anslutningen: Testa SSH-anslutningen genom att logga in på målenheten med SSH från din lokala dator:

```bash
ssh username@target_machine_ip
```

Du bör nu kunna logga in utan att ange ett lösenord eftersom SSH-nyckeln används för autentisering.

Du behöver inte göra några ytterligare konfigurationer för SSH i Ansible eftersom SSH är standardprotokollet. Ansible kommer att använda SSH-nyckeln för autentisering när du kör dina automatiseringsuppgifter.


3. **Konfigurerar sudo på målenheten**: Använd följande tillvägagångssätt om du behöver konfigurera sudo på målenheterna:

Inte sällan konfigurerar man av säkerhetsskäl SSH-servern på så sätt att root inte kan logga in. Om så är fallet måste du använda sudo för många typer av uppgifter. För att säkerställa att du kan utföra uppgifter som kräver root-privilegier utan att logga in som root måste du lägga till använderen som Ansible använder till din sudo konfiguration. Du kan använda kommandot nedan, eller använda din favoriteditor.

```
visudo /etc/sudoers.d/ansibleuser
```

Lägg till följande text:

```
ansibleuser ALL=(ALL) NOPASSWD: ALL
```

Ersätt 'ansibleuser' med användarnamnet för din ansibleanvändare.

RedHat Enterprise Linux, och dess kloner, har SELinux aktiverat som standard. Om inte ovanstående sudo konfiguration fungerar kan du istället lägga till följande rad:

```
ansibleuser ALL=(ALL) ROLE=sysadm_r TYPE=sysadm_t NOPASSWD: ALL
```

När du har följt dessa steg och säkerställt att SSH-anslutningen fungerar korrekt, är din Ansible-konfiguration redo att användas för att automatisera uppgifter på dina målenheter.

## Uppgift

Skapa en katalog som du skall använda för ditt Ansible-projekt. Denna katalog benämns fortsättningsvis projektets rotkatalog. Under denna katalog skapar du följande kataloger:

- files
- templates
- defaults
- roles
- vars
- handlers
- tasks

Du kan också köra nedastående bash-script från din rotkatalog:

```bash
#!/bin/bash

for d in files templates inventories; do
  mkdir -p $d
done

for d in defaults roles vars handlers tasks; do
  mkdir -p $d
  echo "# This is the default configuration file for $d" > $d/main.yml
done
```

Under inventories katalogen skall du skapa en inventariefil i INI-format med filnamnet main.ini med de målenheter du fått dig tilldelade. Den ena måleneten skall tillhöra gruppen 'web', och den andra gruppen 'db'.

Skapa därefter SSH-anslutningar till målenheterna.

[Lösning](solutions/ansible_s2.md)

Föregående: [Introduktion till automatisering med Ansible](ansible_s1.md)

Nästa: [YAML Strukturerad Datahantering](ansible_s3.md)



# Lektion 6

# Arbeta med moduler i Ansible

Under denna lektion studerar vi Ansible-moduler och deras centrala roll inom automatisering. Moduler används för att utföra vanliga uppgifter och kan vid behov användas i villkor och loopar i playbooks.

### Utforska Ansible-moduler och deras roll i automatisering

**Ansible-moduler** är små bitar av kod som används för att utföra specifika uppgifter på målenheter. Moduler gör det möjligt att automatisera många typer av uppifter, och hanterar allt från pakethantering till filhantering och användaradministration.

Modulerna kan anropas i en playbook-fil för att utföra uppgifter på de målenheter som definierats. När Ansible kör en playbook, skickas instruktioner till varje målenhet med hjälp av modulerna.

### Moduler för vanliga uppgifter

Låt oss börja med några av de vanligaste uppgifterna som vi kan automatisera med hjälp av moduler.

#### Pakethantering

Använd modulerna [`dnf`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/dnf_module.html), [`yum`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/yum_module.html), [`apt`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_module.html) eller [`package`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/package_module.html) beroende på din Linux-distribution och behov för att hantera programinstallation och uppdateringar.

```yaml
- name: Install Apache
  dnf:
    name: httpd
    state: present
```

I exemplet ovan används `dnf`-modulen för att installera paketet `httpd` på en målenhet. Om du använder en Linux-distribution som inte har stöd för dnf, kan du använda modulen `package`. Denna modul ger en bredare kompatibilitet för olika Linux-distributioner. Här är ett exempel med `package`:

```yaml
- name: Install Apache using the generic package module
  package:
    name: httpd
    state: present
```

För Windows målenheter kan du använda modulen [`win_package`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_package_module.html) för att hantera programinstallation och uppdateringar. Här är ett exempel:

```yaml
- name: Install Notepad++
  win_package:
    name: notepadplusplus
    state: present
```

#### Filhantering

Kopiera, ta bort eller ändra filer på målenheter med modulerna [`copy`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/copy_module.html), [`file`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html) och [`template`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/template_module.html).

```yaml
- name: Copy a configuration file
  copy:
    src: /local/configurationfile.conf
    dest: /etc/configurationfile.conf
```

Ovanståennde kod med `copy`-modulen kopierar en lokal konfigurationsfil till en målenhets målmapp.

Windows-system kan hanteras med modulerna [`win_copy`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_copy_module.html) och [`win_file`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_file_module.html) för att kopiera, ta bort eller ändra filer på målenheter. Till exempel:

```yaml
- name: Copy a configuration file to Windows
  win_copy:
    src: C:\local\configurationfile.conf
    dest: C:\configurationfile.conf
```

Ovanstående kod med win_copy-modulen kopierar en lokal konfigurationsfil till en Windows-målenhets målmapp.

#### Användaradministration

Skapa, ändra eller ta bort användarkonton med modulerna [`user`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/user_module.html) och [`group`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/group_module.html).

```yaml
- name: Create a new user
  user:
    name: new_user
    state: present
```

`user`-modulen skapar i exemplet ovan en ny användare på målenheten med användarnamnet `new_user`.

För att administrera användarkonton på Windows-målenheter, använd modulen [`win_user`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_user_module.html):

```yaml
- name: Create a new user on Windows
  win_user:
    name: new_user
    state: present
```

`win_user`-modulen skapar i detta fall en ny användare på Windows-målenheten med användarnamnet `new_user`.

#### Konfigurationshantering

Modifiera konfigurationsfiler med moduler som [`ini_file`](https://docs.ansible.com/ansible/latest/collections/community/general/ini_file_module.html), [`lineinfile`](https://docs.ansible.com/ansible/latest/collections/community/general/lineinfile_module.html) och [`blockinfile`](https://docs.ansible.com/ansible/latest/collections/community/general/blockinfile_module.html).

```yaml
- name: Modify configuration
  lineinfile:
    path: /etc/configurationfile.conf
    line: 'new_configuration_line'
```

`lineinfile`-modulen kan användas för att lägga till eller ändra en rad i en befintlig konfigurationsfil.

#### Nätverkskonfiguration

Hantera nätverksinställningar och brandväggar med moduler som `ios_*` (för Cisco-enheter) och [`iptables`](https://docs.ansible.com/ansible/latest/collections/community/general/iptables_module.html).

```yaml
- name: Configure firewall
  iptables:
    chain: INPUT
    rule: -p tcp --dport 80 -j ACCEPT
```

`iptables`-modulen skapar med ovanstående kod en ny regel i brandväggen som tillåter inkommande trafik till port 80.

#### Databashantering

Interagera med databaser och utför SQL-frågor med moduler som [`mysql_db`](https://docs.ansible.com/ansible/latest/collections/community/mysql/mysql_db_module.html) och [`postgresql_db`](https://docs.ansible.com/ansible/latest/collections/community/postgresql/postgresql_db_module.html).

```yaml
- name: Create a database
  mysql_db:
    name: new_database
    state: present
```

`mysql_db`-modulen används för att skapa en ny databas med namnet `new_database`.

Modulerna som avser Windows ingår inte i ansible-core. Du kan kontrollera om dessa moduler är installerade med följande kommando:

```bash
ansible-galaxy collection list|grep windows
```

### Använda villkor och loopar i playbooks

Villkor och loopar ger dig möjlighet att styra exekveringen i dina playbooks beroende på olika förhållanden och att upprepa uppgifter flera gånger.

#### Villkor

Använd `when`-nyckelordet för att inkludera villkor för att avgöra om en uppgift ska utföras eller inte. Till exempel:

```yaml
- name: Start Apache if it's not running
  service:
    name: httpd
    state: started
  when: apache_status.status != 'active'
```

I detta exempel kommer uppgiften att utföras om tillståndet för Apache inte är 'active'.

#### Loopar

Loopar kan användas för att upprepa en uppgift flera gånger. Till exempel, om du vill installera flera paket:

```yaml
- name: Install multiple packages
  package:
    name: "{{ item }}"
    state: present
  loop:
    - package1
    - package2
    - package3
```

I det här fallet kommer uppgiften att köras tre gånger, en gång för varje paket i listan.

Med dessa koncept kan du skapa mer dynamiska och anpassningsbara playbooks för att automatisera komplexa uppgifter och arbetsflöden.

### Uppgift

Utforska Ansible-moduler genom att skapa en playbook som installerar "mariadb", "mariadb-backup" och "mariadb-server-utils" på målenheterna i gruppen "db". Använd dig av inventariefilerna från uppgiften i föregående lektion. Uppdatera hosts.yml filen i "development" miljön med ip-adresserna till målenheterna i din testmiljö. Skriv din playbook i YAML-format, och använd en loop för att installera paketen. Kör den sedan mot din "development" miljö.

[Lösning](solutions/ansible_s6.md)

Föregående: [Hantera inventariefiler i komplexa miljöer](ansible_s5.md)

Nästa: [Hantera roller och mallar i Ansible](ansible_s7.md)


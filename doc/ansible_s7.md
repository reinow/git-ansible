# Lektion 7

# Hantera roller och mallar i Ansible

I denna lektionen kommer vi att utforska två nyckelkoncept i Ansible: hantering av [roller (roles)](https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html) och användning av [mallar (templates)](https://docs.ansible.com/ansible/latest/user_guide/playbooks_templating.html) med [Jinja2](https://palletsprojects.com/p/jinja/). Roller och mallar har stor betydelse för att skapa välstrukturerade och återanvändbara automatiseringslösningar.

### Skapa och Organisera Roller

**Roller (roles)** är kärnan i Ansible's struktur. De möjliggör en elegant och modulär metod för att organisera din automatiseringskod. Genom att skapa och använda roller kan du separera olika delar av din automatisering och återanvända delarna effektivt inom flera olika projekt. Istället för att skapa komplexa och svårförståeliga playbooks med många uppgifter och moduler, kan du använda roller för att dela upp din automatisering i logiska och återanvändbara enheter. Roller gör det också möjligt att inkludera kompletta uppsättningar av uppgifter, variabler och mallar för specifika ändamål, som att konfigurera en webbserver eller en databasserver.

#### Skapa en Roll

För att skapa en roll i Ansible, använd kommandot `ansible-galaxy init` följt av namnet på rollen. Till exempel:

```bash
ansible-galaxy init my_role
```

Detta kommando skapar en katalogstruktur för din roll.

#### Rollens Katalogstruktur

En roll i Ansible följer en specifik katalogstruktur. Här är en översikt över en typisk katalogstruktur för en Ansible-roll:

```
.
└── my_role
    ├── defaults
    │   └── main.yml
    ├── files
    ├── handlers
    │   └── main.yml
    ├── meta
    │   └── main.yml
    ├── README.md
    ├── tasks
    │   └── main.yml
    ├── templates
    ├── tests
    │   ├── inventory
    │   └── test.yml
    └── vars
        └── main.yml
```

Låt oss utforska varje katalog:

- `defaults`: Här kan du definiera standardvariabler för din roll. Dessa variabler används om inte samma variabel är definierad i katalogen `vars`. Om en variabel är definierad både i `defaults` och i `vars`, kommer värdet på variabeln i `vars` att användas.

- `files`: Platsen för statiska filer som behöver kopieras till dina målenheter.

- `handlers`: I `handlers`-katalogen definierar du hanterare (handlers). Ibland vill du att en uppgift ska köras endast när en förändring utförts på en målenhet. Till exempel kan du vilja starta om en tjänst om en uppgift uppdaterar konfigurationen för den tjänsten, men inte om konfigurationen är oförändrad. Ansible använder hanterare (handlers) för att hantera detta förfarande. Hanterare är uppgifter som endast körs när de notifieras.

- `meta`: Innehåller metadata om rollen, såsom upphovsrätt, beroenden och plattformar som stöds.

- `README.md`: Används för dokumentation av rollen, inklusive instruktioner, krav och annan viktig information.

- `tasks`: Här definierar du huvuduppgifterna (tasks) för rollen. Dessa uppgifter beskriver de konkreta åtgärder som din roll kommer att utföra på målenheterna.

- `templates`: Används för att skapa dynamiska konfigurationsfiler med Jinja2-mallar.

- `tests`: Här kan du inkludera filer som används för att testa rollen, inklusive inventariefiler och testplaybooks.

- `vars`: I denna katalog definierar du variabler som används av rollen men som inte anses som ett standardvärde för den aktuella variabeln. En variabel som är definierad i `vars` katalogen får företräde över en variabel med samma namn som är definierad i playbooken eller `defaults` katalogen. På så sätt är det  möjligt att anpassa rollen utan att ändra kärnkoden i rollen.

### Skapa återanvändbara roller för konfigurationshantering

Förmågan att skapa **roller** (roles) i Ansible är viktigt för återanvändning och modularitet. Istället för att duplicera kod i olika playbooks kan du enkelt återanvända dina roller i flera projekt.

För att skapa en roll i Ansible, följ dessa steg:

1. Skapa rollens katalogstruktur (se ovan).
   
2. Definiera rollens uppgifter (tasks) i `tasks`-katalogen. Dessa beskriver de konkreta åtgärder som rollen kommer att utföra på målenheterna.

```yaml
# In tasks/main.yml
- name: Ensure Apache is installed
  package:
    name: httpd
    state: present

- name: Ensure Apache is running
  systemd:
    name: httpd
    state: started
  become: yes
```

I detta exempel definierar vi två uppgifter för rollen att utföra. Den första uppgiften ser till att Apache-paketet är installerat, och den andra uppgiften ser till att Apache-tjänsten körs. "Become: yes" i den andra uppgiften indikerar att Ansible ska eskalera sina privilegier, om det är nödvändigt, för att utföra uppgiften och säkerställa att Apache körs.

3. Utveckla din roll efter behov. Du kan utveckla och anpassa rollen genom att ändra variabler i `defaults/main.yml`, lägga till filer i `files`-katalogen och skapa Jinja2-mallar i `templates`-katalogen.

4. Återanvänd din roll i olika projekt och playbooks genom att inkludera den som ett beroende.

För att ange sökvägen till en roll i Ansible kan du använda en av följande metoder:

1. **Relativ sökväg**: Om rollen du vill inkludera ligger i samma katalog som din playbook eller i en underkatalog till din playbook kan du ange den med en relativ sökväg. Till exempel, om din playbook ligger i samma katalog som rollkatalogen kan du inkludera rollen så här:

   ```yaml
   - hosts: localhost
     roles:
       - my_role
   ```

   Om din roll ligger i en underkatalog kan du specificera sökvägen relativt till din playbook:

   ```yaml
   - hosts: localhost
     roles:
       - roles/min_underkatalog/min_roll
   ```

2. **Absolut sökväg**: Du kan använda en absolut sökväg för att ange platsen för en roll. Detta är användbart när din roll finns i en annan katalog utanför din playbook's katalogstruktur. Till exempel:

   ```yaml
   - hosts: localhost
     roles:
       - /path/to/your/role
   ```

3. **Ansible Rollers Sökväg**: Ansible har en standard sökväg där Ansible söker efter roller. Som standard är det katalogen "roles" relativt till din playbook. Så om din roll ligger i katalogen "roles" hittar Ansible den automatiskt utan att du behöver ange hela sökvägen:

   ```yaml
   - hosts: localhost
     roles:
       - my_role
   ```

Om du vill ange en annan sökväg till dina roller kan du ange sökvägen i din Ansible-konfigurationsfil (`ansible.cfg`) med hjälp av konfigurationsalternativet `roles_path`:

```ini
[defaults]
roles_path = /path/to/your/role_directory
```

Med denna konfiguration behöver du inte ange sökvägen till din roll i din playbook, eftersom Ansible automatiskt kommer att leta i den angivna katalogen.

Välj den metod som passar bäst för din projektkatalogstruktur och dina behov. Den relativa sökvägen och den standard sökvägen används oftast eftersom de förenklar hanteringen av roller och gör playbook mer portabla.

Att organisera din Ansible-kod med roller möjliggör skapandet av en samling återanvändbara moduler som kan användas över flera projekt och bidrar till konsistens i din automatisering.

### Använda Jinja2-mallar för dynamisk konfigurationsgenerering

Jinja2 är en kraftfull mallmotor som möjliggör dynamisk generering av konfigurationsfiler i Ansible. Med Jinja2 kan du inkludera variabler, använda villkor och loopar i dina mallar för att skapa konfigurationsfiler som är anpassade för  varje målenhet.

För att använda Jinja2 i Ansible, följ dessa steg:

1. Skapa en Jinja2-mall i `templates`-katalogen med filändelsen `.j2`.

2. Använd variabler i dina mallar genom att använda dubbla måsvingar `{{ }}`. Var noga med att definiera och göra variabler tillgängliga i den roll eller playbook som används i mallen.

3. Använd villkor och loopar i Jinja2 för att skapa dynamiska strukturer i dina konfigurationsfiler.

I nedanstående exempel gör vi anpassningar av en roll. I defaults/main.yml anger vi standardvariabler för rollen, såsom HTTP-porten och sökvägen till konfigurationsfilen. I templates/httpd.conf.j2 skapar vi en Jinja2-mall för Apache-konfigurationsfilen som använder dessa variabler. I files/index.html lägger vi till en statisk HTML-fil som kan kopieras till målenheterna.

```yaml
# In defaults/main.yml
httpd_port: 80
httpd_config_path: /etc/httpd/conf/httpd.conf

# In templates/httpd.conf.j2
# This is a simplified example
Listen {{ httpd_port }}

# In files/index.html
# This is a simplified example
<html>
  <head>
    <title>Welcome to My Website</title>
  </head>
  <body>
    <h1>Hello, World!</h1>
  </body>
</html>
```

Med användning av Jinja2-mallar blir dina konfigurationsfiler mer dynamiska och anpassade efter de specifika behoven hos varje målenhet.

### Handlers

**Handlers i Ansible** är uppgifter (tasks) som körs endast när de notifieras av andra uppgifter i rollen eller playbooken. De används ofta för att hantera situationer där en åtgärd, som att starta om en tjänst, endast bör utföras om en ändring har gjorts på målenheten. Med handlers får man en strukturerad och kontrollerad metod för att hantera åtgärder som ska inträffa som en följd av ändringar på målenheten.

Här beskrivs hur handlers fungerar:

1. När en uppgift (task) utförs och resulterar i en ändring på målenheten, kan den notifiera en eller flera handlers om denna ändring.

2. Notifieringsprocessen innebär att handlers lagras i en lista för att exekveras vid ett senare tillfälle.

3. Efter att alla uppgifter (tasks) har körts, kontrollerar Ansible om det finns några handlers som har blivit notifierade. Om det finns handlers i listan som har blivit notifierade, körs de i en sekvens.

Här är ett exempel som visar hur handlers kan användas i en roll:

```yaml
- name: Example role with handlers
  hosts: localhost
  tasks:
    - name: Modify configuration file
      template:
        src: config.j2
        dest: /etc/config.conf
      notify: Restart service

  handlers:
    - name: Restart service
      systemd:
        name: my_service
        state: restarted
```

I exemplet ovan, när uppgiften "Modify configuration file" utförs och resulterar i en ändring, kommer den att **notifiera** hanteraren "Restart service". Efter att alla uppgifter har körts kommer Ansible att kontrollera om någon handler har blivit notifierad, och i så fall kommer hanteraren "Restart service" att exekveras och tjänsten "my_service" kommer att startas om.

Det är viktigt att notera att handlers exekveras **efter** att alla uppgifter i en roll eller playbook har körts. Detta säkerställer att ändringar som kräver en omstart eller andra åtgärder bara utförs en gång i slutet av exekveringen och inte flera gånger.

### Använda dina roller

För att använda din roll i andra projekt, kan du inkludera den som ett beroende. Det gör du genom att specificera rollen under `dependencies` i antingen projektets playbook eller i `meta/main.yml` i en annan roll inom projektet:

**I playbook.yml eller i meta/main.yml för en annan roll:**

```yaml
dependencies:
  - { role: my_role }
```

Med roller och Jinja2-mallar kan du automatisera komplexa uppgifter med precision och återanvändbarhet, och bygga robusta  automatiseringslösningar med Ansible.

### Exekvera din playbook

Om du har skapat en inventariefil kör du  din playbook med följande kommando:

```bash
ansible-playbook -i your_inventory.yml my_playbook.yml
```

## Uppgift

Skapa en roll, nginx_vhost, för att installera webbservern Nginx, och för att konfigurera en virtuell server för domänen example.com. Skapa en playbook som använder rollen, och kör den mot din testmiljö. En enkel konfigurationsfil för Nginx finner du nedan. Skapa en mall (template) med följande variabler: server_name, root, access_log och error_log. Konfigurationsfiler för virtuella servrar i Nginx sparas i katalogen /etc/nginx/conf.d/ 

```
server {
    listen 80;
    server_name example.com;
    root /var/www/example.com;

    index index.html;

    access_log /var/log/nginx/example.com.access.log;
    error_log /var/log/nginx/example.com.error.log;

    location / {
        try_files $uri $uri/ =404;
    }
}
```

[Lösning](solutions/ansible_s7.md)

Föregående: [Arbeta med moduler i Ansible](ansible_s6.md)


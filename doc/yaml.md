# Lektion 3

## YAML Strukturerad Datahantering

YAML, förkortning för "YAML Ain't Markup Language," är ett kraftfullt textbaserat format för att representera strukturerad data. Det används för att organisera och dela information i en tydlig och flexibel hierarkisk struktur. YAML innehåller inte några kommandon eller exekverbara instruktioner.

YAML utvecklades ursprungligen för att vara enkelt att läsa och skriva, samtidigt som det möjliggör en hierarkisk representation av data. YAML är snarlikt JSON (JavaScript Object Notation) och XML (eXtensible Markup Language) när det gäller att lagra och överföra data mellan olika system. JSON är en delmängd av YAML YAML är särskilt användbart när människor behöver granska och hantera datafiler. Det är enkelt att konvertera mellan YAML och JSON, vilket ger flexibilitet i användningen av dessa format. Ett av de mest vanliga användningsområdena för YAML är att skapa konfigurationsfiler. YAML används av automatiseringsverktyget Ansible för att skapa automatiseringsprocesser, samt inom Kubernetes.

### Syntax

YAML använder en intuitiv och enkel syntax som är lätt att tolka för både människor och IT-system. Grundläggande principer i YAML-syntaxen är följande:

1. **Indrag**: YAML använder indrag med mellanslag (inte tabbar) för att ange hierarki och struktur. Varje nivå av indrag representerar en kapslad nivå i datastrukturen. Vanligtvis används två eller fyra mellanslag för varje nivå av indrag.

```yaml
person:
  name: Alice
  age: 30
```

I exemplet ovan är "name" och "age" kapslade inom "person."

2. **Nyckel och värde**: I YAML representeras data i form av nycklar och värden. Nyckeln följs av ett kolon och ett mellanslag, och därefter följer värdet för den aktuella nyckeln. Nedan är ett exempel med data från en enkel bloggpost. YAML kräver inte användning av citattecken omkring enkla strängar. Dock kan citattecken användas för bättre läsbarhet när man hanterar strängar som innehåller specialtecken eller mellanslag. I följande exempel har vi inkluderat värden inom citattecken.

```yaml
post:
  title: "An Exploration of YAML"
  author: "Alice Blogger"
  date: "2023-09-27"
  category: "Technology"
  content: "In this blog post, we explore the use of YAML for managing structured data. YAML is a powerful tool that makes it easy to organize and share information."
```

3. **Kommentarer**: Kommentarer i YAML börjar med ett hashtag-symbol (#) och används för att ge förklaringar eller sammanhang till data.

```yaml
# This is a comment.
name: Alice
  age: 30
```

### Datatyper

YAML stöder ett antal datatyper för att representera olika typer av information. Här är en översikt över de viktigaste datatyperna:

- **Strängar (Strings)**: Strängar är sekvenser av tecken och används för att representera text.

```yaml
message: Hello, this is a string.
```

- **Heltal och flyttal (Numbers)**: Numeriska värden kan vara antingen heltal eller flyttal och används för att representera kvantitativ information.

```yaml
age: 30
price: 19.99
```

- **Boolean (Boolean values)**: Boolean-värden representerar data som används i logiska uttryck. De kan anta värdena "true" eller "false."

```yaml
is_enabled: true
is_admin: false
```

- **Listor (Lists)**: Listor är ordnade samlingar av värden, vilket innebär att de bibehåller ordningen för de element de innehåller.

```yaml
fruits:
  - apple
  - banana
  - orange

fruits2: ['apple', 'banana', 'orange']
```

- **Referenser (Maps)**: Referenser är hierarkiska strukturer som används för att representera objekt med nycklar och värden.

```yaml
person:
  name: Alice
  age: 30
```

Dessa datatyper ger YAML dess flexibilitet och användbarhet i en mängd olika scenarier. Det är viktigt att notera att YAML inte har en inbyggd binär datatyp, men det kan förekomma textbaserade representationer som Base64 för binär data i YAML-dokument.

### Ankare och alias

Med ankare och alias kan man identifiera en post med en ankare i ett YAML-dokument och sedan hänvisa till den posten med ett alias senare i samma dokument. Ankare identifieras av ett &-tecken och alias av ett *-tecken. Här är ett exempel som visar en post i en lista som identifieras av ett ankare med namnet "flag" och som sedan refereras till senare i listan.

```yaml
  - &flag Apple
  - Orange
  - Grape
  - Lemon
  - *flag
```

### Användningsområden för YAML

YAML har ett brett användningsområde. Här är några av de vanligast förekommande:

#### Konfigurationsfiler

Ett av de mest vanliga användningsområdena för YAML är att konfigurera datorprogram och IT-system. YAML-formatet gör det enkelt att skapa och redigera konfigurationsfiler. Till exempel kan en webbapplikation använda en YAML-konfigurationsfil för att ange databasanslutningsparametrar, URL-routning och autentisering.

```yaml
# Configuration file for a web application
database:
  server: localhost
  port: 5432
  username: user
  password: secret
```

#### Datautbyte

YAML används ofta för att utbyta data mellan olika applikationer eller system. Eftersom YAML är läsbart för människor, kan det också vara ett bra format för att dela data med andra utvecklare eller användare. Exempelvis kan du skicka data över nätverket i YAML-format eller exportera data från en applikation till en läsbar YAML-fil.

```yaml
# Data exchange format for a contact list.
contacts:
  - name: Alice
    email: alice@example.com
  - name: Bob
    email: bob@example.com
```

#### Automatiseringsskript

YAML kan också användas för att skapa automatiseringsskript och konfigurationer för olika arbetsflöden. Till exempel kan YAML användas för att beskriva hur en Continuous Integration (CI) eller Continuous Deployment (CD) -pipeline ska konfigureras och utföras.

```yaml
# Automation script for a CI/CD pipeline.
steps:
  - name: Build
    command: npm build
  - name: Test
    command: npm test
  - name: Deploy
    command: scp -r build/ user@example.com:/var/www/project
```

#### Dokumentation

YAML används också ofta för att skapa dokumentation och metadata, som projektets struktur, licensinformation och versionshistorik, för projekt och applikationer.

```yaml
# Project metadata and documentation.
project:
  name: My project
  version: 1.0.0
  license: MIT
  description: An example of an application.
```

### Stöd för YAML i Programmeringsspråk och Redigeringsverktyg

Ett av YAML:s styrkor är dess breda stöd i olika programmeringsspråk och redigeringsverktyg. Det finns YAML-parser och serialiserare tillgängliga för de flesta populära programmeringsspråk, vilket gör det enkelt att integrera YAML-stöd i applikationer.

#### YAML i Programmeringsspråk

Många programmeringsspråk erbjuder bibliotek eller moduler för att enkelt läsa och skriva YAML-filer. Till exempel finns det YAML-bibliotek för Python, Ruby, Java och många andra språk. Detta gör det enkelt att använda YAML för att konfigurera och hantera data i applikationer.

#### YAML i Redigeringsverktyg

Flera populära källkodsredigeringsverktyg och olika integrerade utvecklingsmiljöer (IDE:er) har stöd för YAML. Detta inkluderar funktioner som syntaxmarkering och automatisk identifiering. Det blir därför lättare att redigera YAML-filer och undvika felaktig syntax.

### Filnamnstillägg för YAML-filer

YAML-filer brukar använda filnamnstillägget .yaml eller .yml. Den officiellt rekommenderade filnamnstillägget för YAML-filer har varit .yaml sedan 2006. Att använda rätt filnamnstillägg är viktigt eftersom det hjälper användare och programvara att identifiera filerna som YAML-format.

## Sammanfattning

YAML är ett kraftfullt format för hantering av strukturerad data på ett läsbart och flexibelt sätt. Dess enkla syntax och breda stöd gör det till ett utmärkt val för konfigurationsfiler, datautbyte, automatiseringsskript och dokumentation. Oavsett användningsområdet kan YAML vara ett bra val för att organisera och dela data på ett effektivt sätt.

## Uppgift

Skapa en YAML-fil med data som representerar en virtuell server i Apache.

Föregående: [Konfigurera Ansible](ansible_s2.md)

Nästa: [Ansible Playbooks och Automatisering](ansible_s4.md)

# Uppgifter Ansible

## Lektion 2

```yaml
web:
  hosts:
    webserver1:
      ansible_host: 192.168.1.101
    # You can add more web servers here if needed

databases:
  hosts:
    dbserver1:
      ansible_host: 192.168.1.102
    # You can add more database servers here if needed
```

Med listor:

```yaml
web:
  hosts:
    - name: webserver1
      ansible_host: 192.168.1.101
    # Add more web servers here as needed

databases:
  hosts:
    - name: dbserver1
      ansible_host: 192.168.1.102
    # Add more database servers here as needed
```

## Lektion 3

```yaml
virtual_server:
  server_name: example.com
  document_root: /var/www/html/example.com
  error_log: /var/log/apache2/error_example.com.log
  access_log: /var/log/apache2/access_example.com.log
  server_alias:
    - www.example.com
  directories:
    - path: /var/www/html/example.com
      options: Indexes FollowSymLinks
      allow_override: All
```

- `server_name` representerar det primära domännamnet för den virtuella servern.
- `document_root` specificerar sökvägen till rotkatalogen för dokument.
- `error_log` och `access_log` anger sökvägarna till fel- och åtkomstloggfiler.
- `server_alias` är en valfri lista med serveralias.
- `directories` är en lista över konfigurationer som är specifika för kataloger, inklusive sökvägen till katalogen, alternativ och inställningar för AllowOverride.

## Lektion 4

```yaml
- name: Install Apache and mod_auth_gssapi
  hosts: web
  tasks:
    - name: Install Apache and mod_auth_gssapi
      dnf:
        name:
          - httpd
          - mod_auth_gssapi
        state: present

    - name: Copy Apache configuration file
      copy:
        src: /path/to/configuration-file.conf
        dest: /etc/httpd/conf.d/desired-configuration-file.conf
        owner: root
        group: root
        mode: '0644'

    - name: Start Apache and enable at boot
      systemd:
        name: httpd
        enabled: yes
        state: started

- name: Check Apache status
  hosts: web
  tasks:
    - name: Check status
      systemd:
        name: httpd
      register: apache_status

    - name: Display Apache status
      debug:
        msg: "Apache is {{ 'running' if apache_status.status == 'active' else 'stopped' }}"
```

## Lektion 5

```yaml
all_servers:
  children:
    development:
      children:
        web:
          hosts:
            web_server1:
              ansible_host: 192.168.1.1
            web_server2:
              ansible_host: 192.168.1.2
          children:
            apache:
              hosts:
                apache_server1:
                  ansible_host: 192.168.1.101
                apache_server2:
                  ansible_host: 192.168.1.102
            nginx:
              hosts:
                nginx_server1:
                  ansible_host: 192.168.1.103
                nginx_server2:
                  ansible_host: 192.168.1.104
        db:
          hosts:
            db_server1:
              ansible_host: 192.168.1.3
            db_server2:
              ansible_host: 192.168.1.4
    staging:
      children:
        web:
          hosts:
            web_server3:
              ansible_host: 192.168.1.5
            web_server4:
              ansible_host: 192.168.1.6
          children:
            apache:
              hosts:
                apache_server3:
                  ansible_host: 192.168.1.105
                apache_server4:
                  ansible_host: 192.168.1.106
            nginx:
              hosts:
                nginx_server3:
                  ansible_host: 192.168.1.107
                nginx_server4:
                  ansible_host: 192.168.1.108
        db:
          hosts:
            db_server3:
              ansible_host: 192.168.1.7
            db_server4:
              ansible_host: 192.168.1.8
    production:
      children:
        web:
          hosts:
            web_server5:
              ansible_host: 192.168.1.9
            web_server6:
              ansible_host: 192.168.1.10
          children:
            apache:
              hosts:
                apache_server5:
                  ansible_host: 192.168.1.109
                apache_server6:
                  ansible_host: 192.168.1.110
            nginx:
              hosts:
                nginx_server5:
                  ansible_host: 192.168.1.111
                nginx_server6:
                  ansible_host: 192.168.1.112
        db:
          hosts:
            db_server5:
              ansible_host: 192.168.1.11
            db_server6:
              ansible_host: 192.168.1.12
```

Samma lösning men med YAML listor:

```yaml
all_servers:
  - development:
    - web:
      - web_server1:
          ansible_host: 192.168.1.1
      - web_server2:
          ansible_host: 192.168.1.2
      - apache:
        - apache_server1:
            ansible_host: 192.168.1.101
        - apache_server2:
            ansible_host: 192.168.1.102
      - nginx:
        - nginx_server1:
            ansible_host: 192.168.1.103
        - nginx_server2:
            ansible_host: 192.168.1.104
      - db:
        - db_server1:
            ansible_host: 192.168.1.3
        - db_server2:
            ansible_host: 192.168.1.4
  - staging:
    - web:
      - web_server3:
          ansible_host: 192.168.1.5
      - web_server4:
          ansible_host: 192.168.1.6
      - apache:
        - apache_server3:
            ansible_host: 192.168.1.105
        - apache_server4:
            ansible_host: 192.168.1.106
      - nginx:
        - nginx_server3:
            ansible_host: 192.168.1.107
        - nginx_server4:
            ansible_host: 192.168.1.108
      - db:
        - db_server3:
            ansible_host: 192.168.1.7
        - db_server4:
            ansible_host: 192.168.1.8
  - production:
    - web:
      - web_server5:
          ansible_host: 192.168.1.9
      - web_server6:
          ansible_host: 192.168.1.10
      - apache:
        - apache_server5:
            ansible_host: 192.168.1.109
        - apache_server6:
            ansible_host: 192.168.1.110
      - nginx:
        - nginx_server5:
            ansible_host: 192.168.1.111
        - nginx_server6:
            ansible_host: 192.168.1.112
      - db:
        - db_server5:
            ansible_host: 192.168.1.11
        - db_server6:
            ansible_host: 192.168.1.12
```

## Lektion 6

```yaml
- name: Perform tasks on web hosts
  hosts: web
  become: yes  # This allows Ansible to escalate privileges if needed

  tasks:
    - name: Update packages using dnf
      dnf:
        name: '*'
        state: latest
      become: yes

    - name: Copy a file to the remote hosts
      copy:
        src: /path/to/local/file.txt
        dest: /path/on/remote/file.txt
        owner: user
        group: group
        mode: '0644'
      become: yes

    - name: Create a new user
      user:
        name: newuser
        state: present
        groups: wheel
      become: yes
```

## Lesson 7

```
nginx_vhost/
├── tasks/
│   └── main.yml
├── templates/
│   └── nginx_vhost.j2
├── defaults/
│   └── main.yml
└── meta/
    └── main.yml
```

defaults/main.yml
```
server_name: mywebsite.com
root: /var/www/mywebsite
access_log: /var/log/nginx/mywebsite_access.log
error_log: /var/log/nginx/mywebsite_error.log
```

tasks/main.yml
```
- name: Install Nginx
  yum:
    name: nginx
    state: present
  become: yes

- name: Copy virtual host configuration template
  template:
    src: nginx_vhost.j2
    dest: /etc/nginx/conf.d/my_website.conf
  notify:
    - Reload Nginx
```

templates/nginx_vhost.j2
```
server {
    listen 80;
    server_name {{ server_name }};
    root {{ root }};

    access_log {{ access_log }};
    error_log {{ error_log }};

    location / {
        index index.html;
    }
}
```

playbook
```
- name: Configure Nginx
  hosts: web
  become: yes
  roles:
    - nginx_vhost

```



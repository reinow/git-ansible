# Lektion 2

# Skapa och konfigurera ett GitLab-projekt

I den här lektionen kommer vi att studera hur du skapar och konfigurerar ett nytt projekt i GitLab, hur du konfigurerar åtkomstuppgifter att användas med GitLab och hur du klonar projektet till din lokala dator.

## Skapa ett nytt projekt i GitLab

1. **Logga in på GitLab:** Om du inte redan är inloggad, gå till [GitLab's inloggningssida](https://gitlab.com/users/sign_in) och logga in med dina användaruppgifter.

2. **Välj plats för projektet:** Navigera till din specifika grupp om du vill skapa projektet under en grupp, eller använd ditt personliga utrymme.

3. **Skapa ett nytt projekt:** Klicka på "New Project" eller "Nytt projekt" -knappen. Ange projektets namn, beskrivning och välj om projektet skall vara offentligt (public) eller privat (private). I denna utbildning använder vi förslagsvis endast privata projekt.

4. **Initialisera med en README:** Du har möjlighet att initialisera ditt projekt med en README-fil. Det kan vara användbart för att skapa ett förslag till beskrivning an projektet.

5. **Skapa projektet:** Klicka på "Create project" eller "Skapa projekt" -knappen för att slutföra skapandet av projektet.

## Installera Git

Om du inte redan har Git installerat på din lokala dator, kan du ladda ner och installera det från ditt operativsystems kodförråd eller [Git's officiella webbplats](https://git-scm.com/downloads).

För att installera Git på en Linux-distribution baserad på RHEL (som CentOS, Rocky, Alma eller Fedora) eller en Linux-distribution baserad på Debian (som Debian eller Ubuntu), kan du använda pakethanteraren som är specifik för denna Linux distribution.

### CentOS/RHEL/Fedora/Alma/Rocky:

Använd `dnf` för att installera Git på RHEL, CentOS, Rocky, Alma eller Fedora:

```bash
sudo dnf install git
```

### Debian/Ubuntu:

Använd `apt` för att installera Git på Debian eller Ubuntu:

```bash
sudo apt-get update
sudo apt-get install git
```

Du kan verifiera installationen genom att kontrollera Git-versionen med följande kommando:

```bash
git --version
```

Detta kommando visar den installerade Git-versionen och bekräftar att Git har installerats korrekt.

## Ange användarnamn och e-post

Om du inte redan har konfigurerat ditt användarnamn och din e-post kan du göra det med följande kommandon i terminalen:

```shell
git config --global user.name "Ditt Användarnamn"
git config --global user.email "din@email.com"
```

## Konfigurera åtkomstuppgifter för GitLab

### Konfigurera SSH-autentisering med GitLab

SSH-autentisering gör det möjligt att säkert interagera med kodförråd i GitLab utan att behöva ange ditt lösenord varje gång du utför kommandon som push, pull och kloning. Här är stegen för att konfigurera SSH-autentisering med GitLab:

#### Skapa ett SSH-nyckelpar

Om du inte redan har ett SSH-nyckelpar på din dator måste du generera ett. Öppna en terminal och använd följande kommando för att skapa ett nytt SSH-nyckelpar:

```shell
ssh-keygen -C "din@email.com"
```

Kommandot ssh-keygen -C "din@email.com" kommer att generera ett SSH-nyckelpar med standardnyckeltypen, som är RSA.

Du kan ockå ange algoritm och nyckelstorlek:

```shell
ssh-keygen -t rsa -b 4096 -C "din@email.com"
```

-t rsa: Specifierar vilken typ av nyckel som ska genereras, i det här fallet RSA. RSA är en välbeprövad vanligt förekommande asymmetrisk krypteringsalgoritm som ofta används för SSH-nycklar.

-b 4096: Specifierar nyckelstorlek, som är 4096 bitar i det här fallet. Nyckelstorleken bestämmer säkerhetsnivån. Generellt sett är större nycklar mer säkra, men de kan också vara långsammare att använda. En 4096-bitars nyckel ger en hög säkerhetsnivå.

Ersätt `"din@email.com"` med din giltiga e-postadress som du använder på GitLab.

Under genereringsprocessen kommer du att bli ombedd att välja var du vill spara nyckeln. Välj standardvärdet (`/hem/ditt-användarnamn/.ssh/id_rsa`) om du inte har bra skäl att ange något annat.

Om du inte har sparat ditt SSH-nyckelpar i standardkatalogen, konfigurera din SSH-klient att peka på katalogen där den privata nyckeln är lagrad. Öppna en terminal och kör dessa kommandon:

```shell
eval $(ssh-agent -s)
ssh-add <sökväg till privat SSH-nyckel>
```

#### Lägg till din SSH-nyckel till ditt GitLab-konto

Kopiera din publika SSH-nyckel till ditt GitLab-konto. Använd följande kommando för att kopiera nyckeln till urklipp:

```shell
cat ~/.ssh/id_rsa.pub | xclip -selection clipboard
```

Om du inte har `xclip` installerat kan du använda `cat` för att visa nyckeln och kopiera den manuellt.

Gå till GitLab och logga in på ditt konto. Välj sedan "Edit Profile" eller "Redigera profil" från din användarmeny. Under "SSH Keys" eller "SSH-nycklar," klicka på  "Add new key", eller "Lägg till ny nyckel" och klistra in din kopierade nyckel i textfältet. Klicka därefter på "Add key" eller "Lägg till nyckel".

För att testa din SSH-konfiguration med GitLab, använd följande kommando:

```shell
ssh -T git@gitlab.com
```

Om din SSH-konfiguration är korrekt kommer du att få följande meddelande:

```shell
Welcome to GitLab, @username!
```

Nu har du konfigurerat SSH-autentisering för ditt GitLab-konto. Du kan nu använda Git över SSH utan att behöva ange ditt lösenord varje gång.

**Observera:** Var noga med att skydda din privata SSH-nyckel, och dela den aldrig med någon. Den privata nyckeln ger åtkomst till dina kodförråd på GitLab. I miljöer där hög säkerthet krävs skyddar man ofta sin privata nyckel med en lösenordsfras.

### Skapa ett åtkomsttoken

Inte sällan används en token för autentisering mot ett API. Vi kommer inte att använda GitLabs API i detta utbildning, men det kan vara bra att veta hur man skapar en token för autentisering. Här är stegen för att skapa en personlig åtkomsttoken i GitLab:

   - Logga in på din GitLab-portal och välj "Settings" eller "Inställningar" från menyn.

   - I den vänstra menyn, klicka på "Access Tokens" eller "Åtkomsttoken".

   - Ange en titel för din åtkomsttoken och välj utgångsdatum och vilka behörigheter (scopes) du vill ge ditt åtkomsttoken. Det är viktigt att välja de behörigheter som är relevanta för ditt användningsområde, till exempel läsåtkomst, skrivåtkomst, eller administrativa rättigheter.

   - Klicka på "Create personal access token" eller "Skapa personlig åtkomsttoken" för att generera token.

   - Kopiera och spara din åtkomsttoken på en säker plats. GitLab kommer inte att visa din token igen.

Var noga med att hålla din åtkomsttoken i tryggt förvar, eftersom den ger behörighet till ditt GitLab-konto.

## Klona ett kodförråd

Gå till projektet som du nyss skapade på GitLab. Klicka på "Clone" -knappen och kopiera den URL som avser SSH. Använd sedan ditt terminalprogram på din lokala dator för att klona projektet genom kommandot `git clone [URL]`. Med detta kommando kommer projektet att klonas till den katalog där du befinner dig på din lokala dator.

```bash
git clone [URL]
```

Efter kloningen kan katalogstrukturen på din lokala dator se ut som:

```bash
tree -d -a
.
└── .git
    ├── branches
    ├── hooks
    ├── info
    ├── logs
    │   └── refs
    │       ├── heads
    │       └── remotes
    │           └── origin
    ├── objects
    │   ├── info
    │   └── pack
    └── refs
        ├── heads
        ├── remotes
        │   └── origin
        └── tags

17 directories
```

Om adressen till ditt kodförråd av någon anledning blivit felaktig, kan du själv ange den korrekta adressen till ditt förråd.

```bash
git remote set-url origin ssh://git@gitlab.com/Oribium/gitlab-ansible.git
```

## Sammanfattning

I den här lektionen har vi studerat hur du skapar ett nytt projekt i GitLab, konfigurerar dina åtkomstuppgifter för smidig interaktion med GitLab och klonar projektet till din lokala dator.

## Uppgift

Skapa ett nytt privat projekt med en README.md fil inom ditt personliga utrymme på GitLab. Konfigurera åtkomstuppgifter för SSH-anslutning till ditt GitLab konto. Skapa en ytterligare fil i markdown-format inom projektet med hjälp av GitLabs webbgränssnitt. Klona därefter projektet till din lokala dator.

## Ytterligare Resurser

För mer information och avancerade funktioner, fördjupa dig i [GitLab's officiella dokumentation](https://docs.gitlab.com/ee/). Du kan också läsa [manualen för git](https://linux.die.net/man/1/git).

Föregående: [Introduktion till versionshantering och GitLab](gitlab_s1.md)

Nästa: [Grunderna i Git](gitlab_s3.md)

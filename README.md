# Kom igång med GitLab och Ansible

### Behärska grundläggande funktioner och automatisering

Välkommen till vår utbildning, "Kom igång med GitLab och Ansible" Oavsett om du har tidigare erfarenhet eller inte, kommer vi att guida dig steg för steg genom de grundläggande funktionerna i GitLab och Ansible.

I GitLab-delen kommer du att lära dig grunderna i versionshantering och samarbete inom projekt. Vi kommer att utforska hur du effektivt kan samarbeta med andra utvecklare, spåra ändringar och hantera konflikter i ett gemensamt projekt.

I Ansible-delen kommer du att studera hur Ansible kan användas för att förenkla uppgifter, automatisera rutinmässiga arbetsflöden och spara tid.

Kursen är praktisk och hands-on. Du kommer att arbeta med verkliga projekt och övningar.

[GitLab](doc/gitlab_s1.md)

[Ansible](doc/ansible_s1.md)
